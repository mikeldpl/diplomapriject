package com.github.mikeldpl.ca.core.extractor;

public abstract class BaseExtractorPoly<IN> extends BaseExtractor<IN> implements ExtractorPoly<IN> {
    private long maxDelayBetweenNotesThatPlayedTogether = MAX_DELAY_BETWEEN_NOTES_THAT_PLAYED_TOGETHER_DEFAULT;

    @Override
    public long getMaxDelayBetweenNotesThatPlayedTogether() {
        return maxDelayBetweenNotesThatPlayedTogether;
    }

    @Override
    public void setMaxDelayBetweenNotesThatPlayedTogether(long maxDelayBetweenNotesThatPlayedTogether) {
        this.maxDelayBetweenNotesThatPlayedTogether = maxDelayBetweenNotesThatPlayedTogether;
    }
}
