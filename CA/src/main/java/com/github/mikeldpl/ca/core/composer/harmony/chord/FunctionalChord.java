package com.github.mikeldpl.ca.core.composer.harmony.chord;

public interface FunctionalChord extends Chord, Comparable<FunctionalChord> {
    ChordType getType();
    int getPosition();
    double getFunction();
    void setFunction(double function);
}
