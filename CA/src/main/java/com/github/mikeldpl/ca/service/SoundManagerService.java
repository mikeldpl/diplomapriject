package com.github.mikeldpl.ca.service;

import com.github.mikeldpl.ca.core.sound.SoundAware;
import com.github.mikeldpl.ca.core.sound.SoundSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class SoundManagerService {

    @Autowired
    private Set<ContainerService<?>> soundSensitives;

    public void setSoundSource(SoundSource soundSource) {
        soundSensitives.stream()
                .flatMap(ContainerService::getContent)
                .filter(o -> o instanceof SoundAware)
                .forEach(o -> ((SoundAware)o).acceptSoundSource(soundSource));
    }
}
