package com.github.mikeldpl.ca.core.composer.melody.accompaniment;

import com.github.mikeldpl.ca.core.composer.melody.Melody;
import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;

public interface Accompaniment {

    void accompany(Melody melody, Chord chord, double temperament);
    void setAccompanimentType(AccompanimentType accompanimentType);
}
