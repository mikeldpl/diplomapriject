package com.github.mikeldpl.ca.core.dst.interpreter.strategy;

import com.github.mikeldpl.ca.core.composer.Composer;

public interface ComposerContainer {

    void setComposer(Composer composer);

    Composer getComposer();
}
