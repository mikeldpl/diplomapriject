package com.github.mikeldpl.ca.core.dst.interpreter.strategy;

import com.github.mikeldpl.ca.core.ca.Ca6Area;
import com.github.mikeldpl.ca.core.composer.CommandComposer;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Visual")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Ca6VisualToComposer extends Ca6ToComposerBase {
    @Override
    protected CommandComposer extractComposerCommand(Ca6Area area) {
        return new CommandComposer();//TODO
    }
}
