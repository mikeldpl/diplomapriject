package com.github.mikeldpl.ca.controller;

import com.github.mikeldpl.ca.core.composer.melody.accompaniment.Accompaniment;
import com.github.mikeldpl.ca.service.ConfigService;
import com.github.mikeldpl.ca.core.composer.Composer;
import com.github.mikeldpl.ca.core.composer.Note;
import com.github.mikeldpl.ca.core.composer.harmony.Harmony;
import com.github.mikeldpl.ca.core.composer.harmony.chord.FunctionalChord;
import com.github.mikeldpl.ca.core.composer.melody.MelodyExtractor;
import com.github.mikeldpl.ca.service.ComposerService;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;

@Controller
public class ComposerController {
    private static final int PERSENTS = 100;
    @FXML
    public ComboBox<String> accompaniment_comboBox;
    @FXML
    private ComboBox<String> melody_comboBox;
    @FXML
    private Slider duration_slider;
    @FXML
    private Slider tact_slider;
    @FXML
    private ComboBox<String> composer_comboBox;
    @FXML
    private ComboBox<String> harmony_comboBox;
    @FXML
    private ComboBox<Note> stage_comboBox;
    @FXML
    private VBox chords_vBox;

    @Autowired
    private ComposerService composerService;
    @Autowired
    private ApplicationContext context;
    @Autowired
    private ConfigService configService;

    @FXML
    public void initialize() throws Exception {
        //------------------------------------------------------------------Composer
        composer_comboBox.getItems().addAll(context.getBeanNamesForType(Composer.class));
        composer_comboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("set Composer = " + newValue);
            configService.setComposer(newValue);
            composerService.setComposer(newValue);
        });
        composer_comboBox.setValue(configService.getComposer());
        //------------------------------------------------------------------MelodyExtractor
        melody_comboBox.getItems().addAll(context.getBeanNamesForType(MelodyExtractor.class));
        melody_comboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("set MelodyExtractor = " + newValue);
            configService.setMelodyExtractor(newValue);
            composerService.setMelodyExtractor(newValue);
        });
        melody_comboBox.setValue(configService.getMelodyExtractor());
        //------------------------------------------------------------------Accompaniment
        accompaniment_comboBox.getItems().addAll(context.getBeanNamesForType(Accompaniment.class));
        accompaniment_comboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("set Accompaniment = " + newValue);
            configService.setAccompaniment(newValue);
            composerService.setAccompaniment(newValue);
        });
        accompaniment_comboBox.setValue(configService.getAccompaniment());
        //------------------------------------------------------------------Composer Duration
        duration_slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("set Composer Duration = " + newValue);
            configService.setComposerDuration(newValue.doubleValue());
            composerService.setDuration(newValue.doubleValue());
        });
        duration_slider.setValue(configService.getComposerDuration());
        //------------------------------------------------------------------Tact
        tact_slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("set Tact = " + newValue);
            configService.setTact(newValue.intValue());
            composerService.setTact(newValue.intValue());
        });
        tact_slider.setValue(configService.getTact());
        //------------------------------------------------------------------Stage
        stage_comboBox.getItems().addAll(Note.values());
        stage_comboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("set Stage = " + newValue);
            configService.setComposerStage(newValue);
            composerService.setStage(newValue);
        });
        stage_comboBox.setValue(configService.getComposerStage());
        //------------------------------------------------------------------ComposerHarmony
        harmony_comboBox.getItems().addAll(context.getBeanNamesForType(Harmony.class));
        harmony_comboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("set ComposerHarmony = " + newValue);
            configService.setComposerHarmony(newValue);
            composerService.setHarmony(newValue);
            refreshHarmony(composerService.getHarmony());
        });
        harmony_comboBox.setValue(configService.getComposerHarmony());
    }

    private void refreshHarmony(Harmony harmony) {
        chords_vBox.getChildren().clear();
        for (FunctionalChord functionalChord : harmony.getCords()) {
            chords_vBox.getChildren().add(createChordView(functionalChord));
        }
    }

    private Node createChordView(FunctionalChord functionalChord) {
        GridPane gridPane = createGridPane();
        Label label = createLabel(functionalChord);
        Slider slider = createSlider(functionalChord);
        slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            double doubleValue = newValue.doubleValue();
            System.out.println("set " + functionalChord.getId() + " chord function = " + doubleValue);
            functionalChord.setFunction(doubleValue);
            configService.putFunctionalCord(functionalChord.getId(), functionalChord.getFunction());
        });
        slider.setValue(configService.getFunctionalCord(functionalChord.getId()));
        gridPane.addRow(0, label, slider);
        return gridPane;
    }

    private GridPane createGridPane() {
        GridPane gridPane = new GridPane();
        gridPane.setPrefWidth(193);
        RowConstraints rowConstraints = new RowConstraints(30);
        rowConstraints.setValignment(VPos.TOP);
        gridPane.getRowConstraints().add(rowConstraints);
        ColumnConstraints column1Constraints = new ColumnConstraints(45);
        column1Constraints.setHalignment(HPos.CENTER);
        gridPane.getColumnConstraints().add(column1Constraints);
        gridPane.getColumnConstraints().add(new ColumnConstraints(148));
        gridPane.setAlignment(Pos.TOP_CENTER);
        return gridPane;
    }

    private Label createLabel(FunctionalChord functionalChord) {
        return new Label(functionalChord.getName());
    }

    private Slider createSlider(FunctionalChord functionalChord) {
        Slider slider = new Slider(0, PERSENTS, functionalChord.getFunction());
        slider.setSnapToTicks(true);
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        slider.setMinorTickCount(0);
        slider.setMajorTickUnit(25);
        return slider;
    }
}
