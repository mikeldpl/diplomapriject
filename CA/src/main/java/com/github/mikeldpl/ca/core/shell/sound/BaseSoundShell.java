package com.github.mikeldpl.ca.core.shell.sound;

import com.github.mikeldpl.ca.consts.MusicConstants;

import java.util.function.IntConsumer;

public abstract class BaseSoundShell implements SoundShell {

    private NoteState[] noteStates = new NoteState[MusicConstants.ALL_NOTES_LENGTH];

    {
        for (int i = 0; i < noteStates.length; i++) {
            noteStates[i] = new NoteState();
        }
    }

    @Override
    public void fire(SoundInfo soundInfo) {
        if (soundInfo != null) {
            fire(soundInfo, a -> {
            });
        }
    }

    @Override
    public void fire(SoundInfo soundInfo, IntConsumer preProcess) {
        for (NoteAction noteAction : soundInfo) {
            consumeNoteAction(noteAction, preProcess);
        }
    }

    protected void consumeNoteAction(NoteAction noteAction, IntConsumer preProcess) {
        byte key = noteAction.key;
        NoteState noteState = noteStates[key];
        synchronized (noteState) {
            if (noteAction.isOn ^ noteState.isLocked()) {
                noteState.lock(noteAction.isOn);
                preProcess.accept(key);
                if (noteAction.isOn) {
                    consumeOn(key);
                } else {
                    consumeOff(key);
                }
            }
        }
    }

    protected abstract void consumeOn(byte key);

    protected abstract void consumeOff(byte key);

    private static class NoteState {
        private boolean lock;

        boolean isLocked() {
            return lock;
        }

        void lock(boolean lock) {
            this.lock = lock;
        }
    }
}
