package com.github.mikeldpl.ca;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.github.mikeldpl.ca.controller",
        "com.github.mikeldpl.ca.service",
        "com.github.mikeldpl.ca.core"})
public class ContextConfig {
}
