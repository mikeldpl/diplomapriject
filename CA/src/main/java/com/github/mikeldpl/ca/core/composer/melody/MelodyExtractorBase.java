package com.github.mikeldpl.ca.core.composer.melody;

public abstract class MelodyExtractorBase implements MelodyExtractor {
    protected int tactDuration;

    @Override
    public void setTactDuration(int tact) {
        this.tactDuration = tact;
    }

    @Override
    public void close() {
    }
}
