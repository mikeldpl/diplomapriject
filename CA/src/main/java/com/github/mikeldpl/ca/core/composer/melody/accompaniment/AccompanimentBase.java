package com.github.mikeldpl.ca.core.composer.melody.accompaniment;

import com.github.mikeldpl.ca.core.composer.melody.Melody;
import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public abstract class AccompanimentBase implements Accompaniment {

    @Autowired
    @Qualifier("Default AccompanimentType")
    private AccompanimentType accompanimentType;

    @Override
    public void setAccompanimentType(AccompanimentType accompanimentType) {
        this.accompanimentType = accompanimentType;
    }

    @Override
    public void accompany(Melody melody, Chord chord, double temperament) {
        melody.shiftToBegin();
        extractChord(melody, accompanimentType.shiftChord(chord), temperament);
    }

    protected abstract void extractChord(Melody melody, Chord chord, double temperament);

}
