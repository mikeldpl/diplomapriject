package com.github.mikeldpl.ca.core.extractor;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.sound.midi.MetaMessage;


@Component("Mono Extractor")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ExtractorMidiMono extends BaseExtractor<MetaMessage> implements ExtractorMidi {
    @Override
    public synchronized void extract(MetaMessage meta) {
        if (MidiMetaMessageUtil.getType(meta) == MidiMetaMessageUtil.NOTE_ON_MESSAGE) {
            addNoteToMessage(MidiMetaMessageUtil.getNote(meta));
            draw();
        }
    }
}
