package com.github.mikeldpl.ca.core.shell.text;

import javafx.application.Platform;
import javafx.scene.control.IndexRange;
import javafx.scene.control.TextArea;
import org.springframework.stereotype.Component;

@Component("default")
public class TextShellTextArea implements TextShell {

    private TextArea textArea;
    private volatile boolean notFollow;

    public void setTextArea(TextArea textArea) {
        this.textArea = textArea;
    }

    public synchronized void fire(String text) {
        Platform.runLater(() -> {
            if (notFollow) {
                IndexRange selection = textArea.getSelection();
                textArea.appendText(text);
                textArea.selectRange(selection.getStart(), selection.getEnd());
            } else {
                textArea.appendText(text);
            }
        });
    }

    public synchronized void setFollow(boolean follow) {
        this.notFollow = !follow;
    }

    @Override
    public void close() {
    }
}