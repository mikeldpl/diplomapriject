package com.github.mikeldpl.ca.core.dst.drawer;

import com.github.mikeldpl.ca.core.ca.CaArea;
import com.github.mikeldpl.ca.core.dst.CaDestination;
import javafx.scene.Group;


public interface CaDrawer<A extends CaArea<?>> extends CaDestination<A> {

    void setDrawArea(Group drawArea);

    void setCellSize(double size);

    void setIsGraphicsSimple(boolean isGraphicsSimple);
}
