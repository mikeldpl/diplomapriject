package com.github.mikeldpl.ca.core.composer.harmony.chord;

public interface Chord {
    String getId();
    String getName();
    int[] getStapes();
}
