package com.github.mikeldpl.ca.core.extractor;

public abstract class BaseExtractorPolyOneThread<IN> extends BaseExtractor<IN> implements ExtractorPolyOneThread<IN> {
    private long maxDelayBetweenNotesThatPlayedTogetherInTicks = MAX_DELAY_BETWEEN_NOTES_THAT_PLAYED_TOGETHER_DEFAULT_IN_TICKS;

    @Override
    public long getMaxDelayBetweenNotesThatPlayedTogetherInTicks() {
        return maxDelayBetweenNotesThatPlayedTogetherInTicks;
    }

    @Override
    public void setMaxDelayBetweenNotesThatPlayedTogetherInTicks(long maxDelayBetweenNotesThatPlayedTogether) {
        this.maxDelayBetweenNotesThatPlayedTogetherInTicks = maxDelayBetweenNotesThatPlayedTogether;
    }
}
