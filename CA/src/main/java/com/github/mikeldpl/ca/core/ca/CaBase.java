package com.github.mikeldpl.ca.core.ca;

import com.github.mikeldpl.ca.core.ObservableBase;
import com.github.mikeldpl.ca.core.dst.CaDestination;
import com.github.mikeldpl.ca.core.extractor.harmonia.Harmonia;
import com.github.mikeldpl.ca.exception.CaException;

import java.util.HashSet;
import java.util.Set;


public abstract class CaBase<A extends CaArea<?>, O extends CaDestination<A>> extends ObservableBase<A, O> implements Ca<A, O> {

    private Set<Integer> livingValues = new HashSet<>();
    private Set<Integer> reviveValues = new HashSet<>();
    private int maxState = 1;
    //	private int numOfPress;
    private Harmonia harmonia;

    @Override
    public void setSize(int i, int size) {
        synchronized (getArea()) {
            getArea().setSize(i, size);
            notifyObservers();
        }
    }

    @Override
    public int getMinSize() {
        return getArea().getMinSize();
    }

    @Override
    public void clear() {
        synchronized (getArea()) {
//			numOfPress=0;
            getArea().clear();
            notifyObservers();
        }
    }

    @Override
    public void setRandomBeginStates() {
        synchronized (getArea()) {
            getArea().setRandomBeginStates();
            notifyObservers();
        }
    }

    @Override
    public Harmonia getHarmonia() {
        return harmonia;
    }

    @Override
    public void setHarmonia(Harmonia harmonia) {
        this.harmonia = harmonia;
    }

    private int[] diapasonToArray(int from, int to) {
        int[] res = new int[to - from + 1];
        for (int i = 0; i < res.length; i++) {
            res[i] = from + i;
        }
        return res;
    }

    @Override
    public void setLivingValues(int... values) {
        livingValues.clear();
        for (int value : values) {
            livingValues.add(value);
        }
    }

    @Override
    public void setLivingDiapason(int from, int to) {
        setLivingValues(diapasonToArray(from, to));
    }

    @Override
    public void setReviveDiapason(int from, int to) {
        setReviveValues(diapasonToArray(from, to));
    }

    @Override
    public void setReviveValues(int... values) {
        reviveValues.clear();
        for (int value : values) {
            reviveValues.add(value);
        }
    }

    protected int getState(int prevState, int livingNeighbors) {
        return prevState + (prevState <= 0 ? (reviveValues.contains(livingNeighbors) ? 1 : 0) :
                (livingValues.contains(livingNeighbors) ? (getMaxState() <= prevState ? getMaxState() - prevState : 1) : -1));
    }

    @Override
    protected A getInfoForObserve() {
        return getArea();
    }

    @Override
    public void inMessage(int[] msg) {
        synchronized (getArea()) {
//			System.out.println(numOfPress++);///to delete
//            System.out.println(Arrays.toString(msg));
            componRules(msg);
            process();
            notifyObservers();
        }
    }

    @Override
    public int getNumberSizes() {
        return getArea().getNumberOfSizes();
    }

    @Override
    public int getMaxState() {
        return maxState;
    }

    @Override
    public void setMaxState(int states) {
        if (states < 1)
            throw new CaException("State of CA can't be less then 1");
        maxState = states;
    }

    public abstract class CellBase implements Cell {
        private int value;

        public CellBase() {
            setDefaultState();
        }

        @Override
        public int getState() {
            return value;
        }

        @Override
        public void setState(int state) {
            state = state < 0 ? 0 : state > getMaxState() ? getMaxState() : state;
            this.value = state;
        }

        @Override
        public int getMaxState() {
            return CaBase.this.getMaxState();
        }
    }
}
