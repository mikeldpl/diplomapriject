package com.github.mikeldpl.ca.core.composer.melody;

import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("No Melody")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MelodyExtractorNone extends MelodyExtractorBase {

    @Override
    public Melody extract(Chord chord, double temperament) {
        return new Melody().addPauseInThisMoment(tactDuration);
    }
}
