package com.github.mikeldpl.ca.core.extractor.harmonia;

import org.springframework.stereotype.Component;

@Component("Snowflake2")
public class Snowflake2 implements Harmonia {
    private static int[] val = new int[]{0, 1, 2, 4, 3, 0, 5, 1, 2, 3, 4, 5};

    //                                0  1  2  3  4  5  6  7  8  9 10 11
    @Override
    public int[] get() {
        return val;
    }
}
