package com.github.mikeldpl.ca.core.shell.sound;

import com.github.mikeldpl.ca.consts.MusicConstants;
import com.github.mikeldpl.ca.core.shell.text.TextShell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.function.IntConsumer;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SimpleSoundToTextShell extends SoundSellMonitor {

    @Autowired
    @Qualifier("default")
    private TextShell textShell;

    @Override
    public void fire(SoundInfo soundInfo, IntConsumer preProcess) {
        textShell.fire("[");
        for (NoteAction noteAction : soundInfo) {
            consumeNoteAction(noteAction, preProcess);
        }
        textShell.fire("]\t");
    }

    @Override
    protected void consumeOn(byte key) {
        textShell.fire(MusicConstants.NOTE_NAMES[key] + " ");
    }

    @Override
    protected void consumeOff(byte key) {
    }
}
