package com.github.mikeldpl.ca.service;

import com.github.mikeldpl.ca.consts.MusicConstants;
import com.github.mikeldpl.ca.core.ca.Ca;
import com.github.mikeldpl.ca.core.extractor.Extractor;
import com.github.mikeldpl.ca.core.transformer.TransformerStrategy;
import com.github.mikeldpl.ca.core.src.instrument.CaInstrument;
import com.github.mikeldpl.ca.exception.CaException;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.stream.Stream;

@Service
public class InstrumentService implements ContainerService<CaInstrument> {
    @Autowired
    private CaInstrument caInstrument;
    @Autowired
    private ApplicationContext applicationContext;
    private byte octave = MusicConstants.FIRST_OCTAVE;
    private byte[] keyToNoteArray = new byte[KeyCode.values().length];
    private Node[] nodes;


    public InstrumentService() {
        byte temp = 0;
        keyToNoteArray[KeyCode.Q.ordinal()] = temp++;
        keyToNoteArray[KeyCode.DIGIT2.ordinal()] = temp++;
        keyToNoteArray[KeyCode.W.ordinal()] = temp++;
        keyToNoteArray[KeyCode.DIGIT3.ordinal()] = temp++;
        keyToNoteArray[KeyCode.E.ordinal()] = temp++;
        keyToNoteArray[KeyCode.R.ordinal()] = temp++;
        keyToNoteArray[KeyCode.DIGIT5.ordinal()] = temp++;
        keyToNoteArray[KeyCode.T.ordinal()] = temp++;
        keyToNoteArray[KeyCode.DIGIT6.ordinal()] = temp++;
        keyToNoteArray[KeyCode.Y.ordinal()] = temp++;
        keyToNoteArray[KeyCode.DIGIT7.ordinal()] = temp++;
        keyToNoteArray[KeyCode.U.ordinal()] = temp++;
        keyToNoteArray[KeyCode.I.ordinal()] = temp++;
        keyToNoteArray[KeyCode.DIGIT9.ordinal()] = temp++;
        keyToNoteArray[KeyCode.O.ordinal()] = temp++;
        keyToNoteArray[KeyCode.DIGIT0.ordinal()] = temp++;
        keyToNoteArray[KeyCode.P.ordinal()] = temp++;
        keyToNoteArray[KeyCode.OPEN_BRACKET.ordinal()] = temp++;
        keyToNoteArray[KeyCode.EQUALS.ordinal()] = temp++;
        keyToNoteArray[KeyCode.CLOSE_BRACKET.ordinal()] = temp++;
        keyToNoteArray[KeyCode.BACK_SPACE.ordinal()] = temp++;
        keyToNoteArray[KeyCode.BACK_SLASH.ordinal()] = temp++;
        keyToNoteArray[KeyCode.NUM_LOCK.ordinal()] = temp++;
        keyToNoteArray[KeyCode.HOME.ordinal()] = temp;
        keyToNoteArray[KeyCode.NUMPAD7.ordinal()] = temp++;
        keyToNoteArray[KeyCode.UP.ordinal()] = temp;
        keyToNoteArray[KeyCode.NUMPAD8.ordinal()] = temp;
        keyToNoteArray[KeyCode.Z.ordinal()] = temp++;
        keyToNoteArray[KeyCode.S.ordinal()] = temp++;
        keyToNoteArray[KeyCode.X.ordinal()] = temp++;
        keyToNoteArray[KeyCode.D.ordinal()] = temp++;
        keyToNoteArray[KeyCode.C.ordinal()] = temp++;
        keyToNoteArray[KeyCode.V.ordinal()] = temp++;
        keyToNoteArray[KeyCode.G.ordinal()] = temp++;
        keyToNoteArray[KeyCode.B.ordinal()] = temp++;
        keyToNoteArray[KeyCode.H.ordinal()] = temp++;
        keyToNoteArray[KeyCode.N.ordinal()] = temp++;
        keyToNoteArray[KeyCode.J.ordinal()] = temp++;
        keyToNoteArray[KeyCode.M.ordinal()] = temp++;
        keyToNoteArray[KeyCode.COMMA.ordinal()] = temp++;
        keyToNoteArray[KeyCode.L.ordinal()] = temp++;
        keyToNoteArray[KeyCode.PERIOD.ordinal()] = temp++;
        keyToNoteArray[KeyCode.SEMICOLON.ordinal()] = temp++;
        keyToNoteArray[KeyCode.SLASH.ordinal()] = temp++;
    }

    public void setCa(Ca ca) {
        caInstrument.setCa(ca);
    }

    public void setInstrumentVoice(String instrumentName) {
        caInstrument.setInstrumentVoice(instrumentName);
    }

    String[] getInstrumentVoices() {
        return caInstrument.getInstrumentVoices();
    }

    @PreDestroy
    public void close() throws Exception {
        caInstrument.close();
    }

    public void setExtractor(String extractor) {
        caInstrument.setExtractor((Extractor) applicationContext.getBean(extractor, caInstrument.getExtractorType()));
    }

    public void setNodes(Node... nodes) {
        this.nodes = nodes;
    }

    private byte getNodeByKeyCode(KeyCode code) {
        return keyToNoteArray[code.ordinal()];
    }

    private void pressNote(byte key) {
        getNoteNode(key).setId("pressedNote");
        caInstrument.keyDown((byte) (key + octave));
    }

    private void releaseNote(byte key) {
        getNoteNode(key).setId("releasedNote");
        caInstrument.keyUp((byte) (key + octave));
    }

    private Node getNoteNode(byte key) {
        return nodes[key % nodes.length];
    }

    public void noteByKeyPressed(KeyCode keyCode) {
        byte keyN = getNodeByKeyCode(keyCode);
        if (keyN >= 0)
            pressNote(keyN);
    }

    public void noteByKeyReleased(KeyCode keyCode) {
        byte keyN = getNodeByKeyCode(keyCode);
        if (keyN >= 0)
            releaseNote(keyN);
    }

    public void noteByNodePressed(Node node) {
        pressNote(getNoteByNode(node));
    }

    public void noteByNodeReleased(Node node) {
        releaseNote(getNoteByNode(node));
    }

    private byte getNoteByNode(Node node) {
        for (byte i = 0; i < nodes.length; i++) {
            if (nodes[i] == node) {
                return i;
            }
        }
        throw new CaException("Invalid node given");
    }

    public byte getOctave() {
        return octave;
    }

    public void setOctave(byte octave) {
        if (octave < 0 || octave > 6)
            throw new CaException("Invalid octave " + octave);
        this.octave = (byte) (octave * MusicConstants.NOTES_LENGTH);
    }

    public void setTransformerStrategy(String transformerStrategy) {
        caInstrument.setTransformerStrategy((TransformerStrategy) applicationContext.getBean(transformerStrategy, caInstrument.getStrategyType()));
    }

    public void setIsTransformer(boolean isTransformer) {
        caInstrument.setIsTransformer(isTransformer);
    }

    @Override
    public Stream<CaInstrument> getContent() {
        return Stream.of(caInstrument);
    }
}
