package com.github.mikeldpl.ca.core;

import java.util.List;


public interface Observable<A, O extends Observer<A>> {
    void addObserver(O obs);

    void removeObserver(O obs);

    void notifyObservers();

    List<O> getObservers();
}
