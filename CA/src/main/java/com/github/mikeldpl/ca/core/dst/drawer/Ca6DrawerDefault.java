package com.github.mikeldpl.ca.core.dst.drawer;

import com.github.mikeldpl.ca.consts.WriterConstants;
import com.github.mikeldpl.ca.core.ca.Ca6;
import com.github.mikeldpl.ca.core.ca.Ca6Area;
import com.github.mikeldpl.ca.exception.CaException;
import com.github.mikeldpl.ca.consts.CaConstants;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Polygon;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

@Component("Default")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Ca6DrawerDefault implements Ca6Drawer {
    private static final double ALFA = Math.PI / 3;
    private static final double ARCTG = 1 / Math.tan(ALFA);
    private static final double SIN = Math.sin(ALFA);
    private static final double BASE_HEIGHT;
    private static final double BASE_WIDTH;
    private static final double[] BASE_POINTS;

    static {
        try {
            Polygon basePoligon = FXMLLoader.load(FullElement6.class.getResource(CaConstants.ELEMENT_FXML));
            BASE_HEIGHT = basePoligon.getLayoutBounds().getHeight();
            BASE_WIDTH = basePoligon.getLayoutBounds().getWidth();
            Object[] arr = basePoligon.getPoints().toArray();
            BASE_POINTS = new double[arr.length];
            for (int i = 0; i < arr.length; i++) {
                BASE_POINTS[i] = (Double) arr[i];
            }
        } catch (IOException e) {
            throw new CaException(e);
        }
    }

    private Group drawArea;
    private Ca6Area oldArea;
    private int[] oldSizes;
    private ArrayList<FullElement6> elementHolder = new ArrayList<>();
    private double cellHeight = BASE_HEIGHT;
    private double cellWidth = BASE_WIDTH;
    private double cellSize = 1D;
    private boolean isGraphicsSimple = !WriterConstants.USE_GRADIENT;

    public boolean isGraphicsSimple() {
        return isGraphicsSimple;
    }

    @Override
    public void setIsGraphicsSimple(boolean isGraphicsSimple) {
        this.isGraphicsSimple = isGraphicsSimple;
        refresh();
    }

    @Override
    public void notifyByObservable(Ca6Area info) {
        synchronized (info) {
            if (isSizeChanged(info) || oldArea != info) {
                oldArea = info;
                if (!Platform.isFxApplicationThread()) {
                    Platform.runLater(() -> reset(info));
                } else {
                    reset(info);
                }
            } else {
                if (!Platform.isFxApplicationThread()) {
                    Platform.runLater(this::refresh);
                } else {
                    refresh();
                }
            }
        }
    }

    public void reset(Ca6Area info) {
        synchronized (info) {
            drawArea.getChildren().clear();
            elementHolder.clear();
            elementHolder.ensureCapacity(info.numberOfCells());
            for (Ca6.Cell6 cell6 : info) {
                FullElement6 fullElement6 = new FullElement6(cell6, CaConstants.NUMBER_OF_DRAWN_PARTS);
                elementHolder.add(fullElement6);
                drawArea.getChildren().addAll(fullElement6.getParts());
            }
        }
    }

    private void refresh() {
        elementHolder.forEach(Ca6DrawerDefault.FullElement6::update);
    }

    private boolean isSizeChanged(Ca6Area info) {
        if (Arrays.equals(oldSizes, info.getSizes())) {
            return false;
        } else {
            oldSizes = Arrays.copyOf(info.getSizes(), info.getSizes().length);
            return true;
        }
    }

    @Override
    public void setDrawArea(Group drawArea) {
        this.drawArea = drawArea;
    }

    @Override
    public void setCellSize(double size) {
        if (size > 1D || size <= 0)
            throw new CaException("Invalid Cell Size " + size);
        cellSize = size;
        cellHeight = BASE_HEIGHT * size;
        cellWidth = BASE_WIDTH * size;
        reset(oldArea);
    }

    private class FullElement6 {

        private double x;
        private double y;
        private Ca6PartPolygon[] parts;
        private Ca6.Cell6 cell;

        public FullElement6(Ca6.Cell6 cell, int numberOfDrawnParts) {
            parts = new Ca6PartPolygon[numberOfDrawnParts];
            this.cell = cell;
            y = cell.getI() * cellHeight / 2;
            x = cell.getJ() * cellWidth + y * ARCTG;
            for (int i = 0; i < parts.length; i++) {
                parts[i] = new Ca6PartPolygon(i);
                parts[i].setOnMousePressed(this::onClick);
            }
            update();
        }

        private void onClick(MouseEvent event) {
            cell.setState((cell.getState() + 1) % (cell.getMaxState() + 1));
            refresh();
        }

        private LinearGradient getLinearGradient(int i, Color stateColor) {
            return new LinearGradient((i & 1), WriterConstants.GRADIENT_OFFSET, (i & 1) ^ 1, WriterConstants.GRADIENT_OFFSET,
                    true, CycleMethod.NO_CYCLE, new Stop(WriterConstants.GRADIENT_OFFSET, stateColor), new Stop(1, stateColor.darker()));
        }


        public FullElement6 update() {
            if (!isGraphicsSimple) {
                for (int i = 0; i < parts.length; i++) {
                    parts[i].setFill(getLinearGradient(i, WriterConstants.STATE_COLORS[cell.getState() + cell.getNeighbors()[i].getState()]));
                }
            } else {
                for (int i = 0; i < parts.length; i++) {
                    parts[i].setFill(WriterConstants.STATE_COLORS[cell.getState() + cell.getNeighbors()[i].getState()]);
                }
            }
            return this;
        }

        public Node[] getParts() {
            return parts;
        }

        private class Ca6PartPolygon extends Polygon {
            public Ca6PartPolygon(int position) {
                super(BASE_POINTS);
                setLayoutX(x);
                setLayoutY(y);
                setScaleX(cellSize);
                setScaleY(cellSize);
                double metr = cellHeight / 2;
                setTranslateX(metr * Math.min(position, 1) * SIN);
                setTranslateY(-metr + metr / 2 * (position == 2 ? 3 : position));
                super.setRotate(Math.toDegrees(ALFA) * position);
            }

        }
    }
}
