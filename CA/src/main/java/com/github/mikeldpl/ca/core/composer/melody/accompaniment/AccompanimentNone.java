package com.github.mikeldpl.ca.core.composer.melody.accompaniment;

import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;
import com.github.mikeldpl.ca.core.composer.melody.Melody;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("None")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AccompanimentNone implements Accompaniment {
    @Override
    public void accompany(Melody melody, Chord chord, double temperament) {
    }

    @Override
    public void setAccompanimentType(AccompanimentType accompanimentType) {
    }
}
