package com.github.mikeldpl.ca.core.dst.drawer;

import com.github.mikeldpl.ca.core.ca.Ca6Area;

public interface Ca6Drawer extends CaDrawer<Ca6Area> {
}
