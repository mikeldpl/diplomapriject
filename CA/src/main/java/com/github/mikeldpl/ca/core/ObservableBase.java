package com.github.mikeldpl.ca.core;

import org.springframework.jmx.access.InvalidInvocationException;

import java.util.LinkedList;
import java.util.List;


public abstract class ObservableBase<A, O extends Observer<A>> implements Observable<A, O> {
    private List<O> observers = new LinkedList<>();

    @Override
    public void addObserver(O obs) {
        observers.add(obs);
    }

    @Override
    public void removeObserver(O obs) {
        observers.remove(obs);
    }

    protected void notifyObservers(A info) {
        for (O observer : observers) {
            observer.notifyByObservable(info);
        }
    }

    @Override
    public void notifyObservers() {
        notifyObservers(getInfoForObserve());
    }

    protected A getInfoForObserve() {
        throw new InvalidInvocationException("This Observable is stateless");
    }

    @Override
    public List<O> getObservers() {
        return observers;
    }
}
