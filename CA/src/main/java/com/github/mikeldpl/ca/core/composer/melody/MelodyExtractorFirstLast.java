package com.github.mikeldpl.ca.core.composer.melody;

import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Power Chord")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MelodyExtractorFirstLast extends MelodyExtractorBase {

    @Override
    public Melody extract(Chord chord, double temperament) {
        return new Melody()
                .addInThisMoment(chord.getStapes()[0], tactDuration)
                .addInThisMoment(chord.getStapes()[chord.getStapes().length - 1], tactDuration);
    }
}
