package com.github.mikeldpl.ca.core.transformer;

import com.github.mikeldpl.ca.core.extractor.ExtractorMidi;
import com.github.mikeldpl.ca.core.ca.Ca;

public interface TransformerStrategyMidi extends TransformerStrategy {
    byte invert(byte note, int command, long tick, Ca ca, ExtractorMidi extractor);
}
