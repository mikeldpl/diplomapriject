package com.github.mikeldpl.ca.core.dst.interpreter.strategy;

import com.github.mikeldpl.ca.core.ca.CaArea;

public interface CaInterpreterStrategy<I extends CaArea<?>, O> {
    O interpret(I area);
}
