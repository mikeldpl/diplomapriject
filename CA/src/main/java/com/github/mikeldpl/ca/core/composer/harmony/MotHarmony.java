package com.github.mikeldpl.ca.core.composer.harmony;

import com.github.mikeldpl.ca.core.composer.harmony.chord.ChordType;
import com.github.mikeldpl.ca.core.composer.harmony.chord.FunctionalChordDefault;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Motorhead")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MotHarmony extends HarmonyBase {//TODO

    private final FunctionalChordDefault t = new FunctionalChordDefault("t", ChordType.M53, getPositionByStage(0), this);
    private final FunctionalChordDefault M = new FunctionalChordDefault("M", ChordType.B53, getPositionByStage(2), this);
    private final FunctionalChordDefault db = new FunctionalChordDefault("db", ChordType.B53, getPositionByStage(6)+1, this);
    private final FunctionalChordDefault VI = new FunctionalChordDefault("VI", ChordType.B53, getPositionByStage(5), this);
    private final FunctionalChordDefault VII = new FunctionalChordDefault("VII", ChordType.B53, getPositionByStage(6), this);

    public MotHarmony() {
        super(0, TON, TON, HALF_TON, TON, TON, HALF_TON);
        this
                .addCords(t, db, VI,VII)
                .addCords(M, VI, VII)
                .addCords(db, t,VI)
                .addCords(VI, db, M, VII, t)
                .addCords(VII, M, VI, t);
    }

    @Override
    public String getName() {
        return "Motorhead";
    }
}