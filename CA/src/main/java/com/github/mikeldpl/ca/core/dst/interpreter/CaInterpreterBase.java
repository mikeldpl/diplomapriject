package com.github.mikeldpl.ca.core.dst.interpreter;

import com.github.mikeldpl.ca.core.ObservableBase;
import com.github.mikeldpl.ca.core.Observer;
import com.github.mikeldpl.ca.core.ca.CaArea;
import com.github.mikeldpl.ca.core.dst.interpreter.strategy.CaInterpreterStrategy;

public abstract class CaInterpreterBase<I extends CaArea<?>, O, L extends Observer<O>, S extends CaInterpreterStrategy<I, O>>
        extends ObservableBase<O, L>
        implements CaInterpreter<I, O, L, S> {

    private S strategy;

    @Override
    public void notifyByObservable(I info) {
        if (strategy != null) {
            notifyObservers(strategy.interpret(info));
        }
    }

    protected S getStrategy() {
        return strategy;
    }

    @Override
    public void setStrategy(S strategy) {
        this.strategy = strategy;
    }
}
