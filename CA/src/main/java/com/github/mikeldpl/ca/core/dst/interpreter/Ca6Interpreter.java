package com.github.mikeldpl.ca.core.dst.interpreter;

import com.github.mikeldpl.ca.core.Observer;
import com.github.mikeldpl.ca.core.ca.Ca6Area;
import com.github.mikeldpl.ca.core.dst.interpreter.strategy.CaInterpreterStrategy;

public interface Ca6Interpreter<O, L extends Observer<O>, S extends CaInterpreterStrategy<Ca6Area, O>>
        extends CaInterpreter<Ca6Area, O, L, S> {
}
