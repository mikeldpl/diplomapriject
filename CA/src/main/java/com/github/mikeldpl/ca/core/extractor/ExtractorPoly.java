package com.github.mikeldpl.ca.core.extractor;

public interface ExtractorPoly<IN> extends Extractor<IN> {
    long MAX_DELAY_BETWEEN_NOTES_THAT_PLAYED_TOGETHER_DEFAULT = 70;

    long getMaxDelayBetweenNotesThatPlayedTogether();

    void setMaxDelayBetweenNotesThatPlayedTogether(long maxDelayBetweenNotesThatPlayedTogether);

}
