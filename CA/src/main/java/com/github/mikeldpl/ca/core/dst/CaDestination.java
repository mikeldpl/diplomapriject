package com.github.mikeldpl.ca.core.dst;

import com.github.mikeldpl.ca.core.Observer;
import com.github.mikeldpl.ca.core.ca.CaArea;

public interface CaDestination<A extends CaArea<?>> extends Observer<A> {
}
