package com.github.mikeldpl.ca.core.composer.harmony.chord;

public enum ChordType implements Chord {
    B53("B53", 0, 4, 7),
    M53("M53", 0, 3, 7),
    Dimin53("Dimin 53", 0, 3, 6),
    Augm53("Augm 53", 0, 4, 8),
    D7("D7", 0, 4, 7, 10),
    Сmaj7("Сmaj7", 0, 4, 7, 11),
    Cmin7("Cmin7", 0, 3, 7, 10),
    CmM7("CmM7", 0, 3, 7, 11),
    Cdim7("Cdim7", 0, 3, 6, 10),
    C75("C-7 (♭5)", 0, 3, 6, 11),
    CM7("C+M7", 0, 4, 8, 11);
    private final String name;
    private final int[] stapes;

    ChordType(String name, int... stapes) {
        this.name = name;
        this.stapes = stapes;
    }


    @Override
    public String getId() {
        return "ChordType." + name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int[] getStapes() {
        return stapes;
    }
}
