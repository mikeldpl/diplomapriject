package com.github.mikeldpl.ca.core.sound;

public interface Mutable {

    void mute(boolean isMuted);
}
