package com.github.mikeldpl.ca.core.extractor.harmonia;

public interface Harmonia {
    int[] get();
}
