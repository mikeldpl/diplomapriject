package com.github.mikeldpl.ca.consts;

public class CaConstants {
    public static final String ELEMENT_FXML = "/views/element6.fxml";
    public static final String DRAWER = "Default";
    public static final int MIN_SIZE = 2;
    public static final int NUMBER_SIZES = 3;
    public static final int DIMENSION = 2;
    public static final int DEFAULT_CELL_STATE = 0;
    public static final int[][] AREA = {{-2, 1}, {-1, 2}, {1, 1}, {2, -1}, {1, -2}, {-1, -1}};
    public static final int NUMBER_OF_DRAWN_PARTS = 3;
    public static final int[] LIVING_VALUES = {2, 3};
    public static final int[] REVIVE_VALUES = {2};
    public static final int[] BEGIN_RULES = {1, 1, 1, 1, 1, 1};
    public static final int MAX_STATE = 1;
    public static final double NON_ZERO_STATE_RANDOM = 0.5;
    public static final double TEST_OFFSET = 0.7;

    private CaConstants() {
    }

}
