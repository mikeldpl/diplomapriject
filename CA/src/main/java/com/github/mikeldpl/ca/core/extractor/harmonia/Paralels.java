package com.github.mikeldpl.ca.core.extractor.harmonia;

import org.springframework.stereotype.Component;

@Component("Paralels")
public class Paralels implements Harmonia {
    private static int[] val = new int[]{0, 1, 2, 3, 4, 0, 5, 2, 1, 4, 3, 5};

    //                                0  1  2  3  4  5  6  7  8  9 10 11
    @Override
    public int[] get() {
        return val;
    }
}
