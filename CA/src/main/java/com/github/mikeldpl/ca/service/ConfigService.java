package com.github.mikeldpl.ca.service;

import com.github.mikeldpl.ca.core.sound.SoundSource;
import com.github.mikeldpl.ca.core.composer.Note;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

@XmlRootElement
public class ConfigService {
    private byte octave = 4;
    private String instrument = "Piano 1";
    private String extractor = "Mono Extractor";
    private File lastFile;
    private String caName = "Ca6";
    private int[] sizes = {10, 10, 10};
    private int livingFrom = 2;
    private int livingTo = 3;
    private int reviveFrom = 2;
    private int reviveTo = 3;
    private int maxState = 1;
    private double cellSize = 1D;
    private boolean simpleGraphics = false;
    private String translatorStrategy = "Default midi transformer strategy";
    private boolean transformered;
    private String harmonia = "StrangeMAIN";
    private SoundSource soundSource = SoundSource.SOURCE;
    private String interpreterStrategy = "State with movement";
    private boolean consoleWrapping = true;
    private boolean consoleFollow = true;
    private String melodyExtractor = "Nirvana - SLTS";
    private double composerDuration = 1;
    private int tact = 4;
    private String composer = "Simple Composer";
    private String composerHarmony = "Major";
    private Note composerStage = Note.C;
    private Map<String, Double> functionalCords = new HashMap<>();
    private String accompaniment = "Reliable Chord";
    private int[] caState;

    public Map<String, Double> getFunctionalCords() {
        return functionalCords;
    }

    public void setFunctionalCords(Map<String, Double> functionalCords) {
        this.functionalCords = functionalCords;
    }

    public SoundSource getSoundSource() {
        return soundSource;
    }

    public void setSoundSource(SoundSource soundSource) {
        this.soundSource = soundSource;
    }

    public String getTranslatorStrategy() {
        return translatorStrategy;
    }

    public void setTranslatorStrategy(String translatorStrategy) {
        this.translatorStrategy = translatorStrategy;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public byte getOctave() {
        return octave;
    }

    public void setOctave(byte octave) {
        this.octave = octave;
    }

    public int getMaxState() {
        return maxState;
    }

    public void setMaxState(int maxState) {
        this.maxState = maxState;
    }

    public int getLivingFrom() {
        return livingFrom;
    }

    public void setLivingFrom(int livingFrom) {
        this.livingFrom = livingFrom;
    }

    public int getLivingTo() {
        return livingTo;
    }

    public void setLivingTo(int livingTo) {
        this.livingTo = livingTo;
    }

    public int getReviveFrom() {
        return reviveFrom;
    }

    public void setReviveFrom(int reviveFrom) {
        this.reviveFrom = reviveFrom;
    }

    public int getReviveTo() {
        return reviveTo;
    }

    public void setReviveTo(int reviveTo) {
        this.reviveTo = reviveTo;
    }

    public String getExtractor() {
        return extractor;
    }

    public void setExtractor(String extractor) {
        this.extractor = extractor;
    }

    public int[] getSizes() {
        return sizes;
    }

    public void setSizes(int[] sizes) {
        this.sizes = sizes;
    }

    public File getLastFile() {
        return lastFile;
    }

    public void setLastFile(File lastFile) {
        this.lastFile = lastFile;
    }

    public String getCaName() {
        return caName;
    }

    public void setCaName(String caName) {
        this.caName = caName;
    }

    public double getCellSize() {
        return cellSize;
    }

    public void setCellSize(double cellSize) {
        this.cellSize = cellSize;
    }

    public boolean isSimpleGraphics() {
        return simpleGraphics;
    }

    public void setSimpleGraphics(boolean simpleGraphics) {
        this.simpleGraphics = simpleGraphics;
    }

    public boolean isTransformered() {
        return transformered;
    }

    public void setTransformered(boolean transformered) {
        this.transformered = transformered;
    }

    public String getHarmonia() {
        return harmonia;
    }

    public void setHarmonia(String harmonia) {
        this.harmonia = harmonia;
    }

    public String getInterpreterStrategy() {
        return interpreterStrategy;
    }

    public void setInterpreterStrategy(String interpreterStrategy) {
        this.interpreterStrategy = interpreterStrategy;
    }

    public boolean isConsoleWrapping() {
        return consoleWrapping;
    }

    public void setConsoleWrapping(boolean consoleWrapping) {
        this.consoleWrapping = consoleWrapping;
    }

    public void setConsoleFollow(boolean consoleFollow) {
        this.consoleFollow = consoleFollow;
    }

    public boolean isConsoleFollow() {
        return consoleFollow;
    }

    public void setMelodyExtractor(String melodyExtractor) {
        this.melodyExtractor = melodyExtractor;
    }

    public String getMelodyExtractor() {
        return melodyExtractor;
    }

    public void setComposerDuration(double composerDuration) {
        this.composerDuration = composerDuration;
    }

    public double getComposerDuration() {
        return composerDuration;
    }

    public void setTact(int tact) {
        this.tact = tact;
    }

    public int getTact() {
        return tact;
    }

    public void setComposer(String composer) {
        this.composer = composer;
    }

    public String getComposer() {
        return composer;
    }

    public void setComposerHarmony(String composerHarmony) {
        this.composerHarmony = composerHarmony;
    }

    public String getComposerHarmony() {
        return composerHarmony;
    }

    public void setComposerStage(Note composerStage) {
        this.composerStage = composerStage;
    }

    public Note getComposerStage() {
        return composerStage;
    }

    public double getFunctionalCord(String name) {
        Double aDouble = functionalCords.get(name);
        return aDouble != null ? aDouble.doubleValue() : 0;
    }

    public void putFunctionalCord(String name, double function) {
        functionalCords.put(name, function);
    }

    public void setAccompaniment(String accompaniment) {
        this.accompaniment = accompaniment;
    }

    public String getAccompaniment() {
        return accompaniment;
    }

    public int[] getCaState() {
        return caState;
    }

    public void setCaState(int[] caState) {
        this.caState = caState;
    }
}