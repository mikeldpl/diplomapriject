package com.github.mikeldpl.ca.core.composer.harmony.chord;

import com.github.mikeldpl.ca.core.composer.harmony.Harmony;

public class FunctionalChordDefault implements FunctionalChord {

    private String name;
    private String id;
    private ChordType chordType;
    private int position;
    private double function;
    private int[] stapes;

    public FunctionalChordDefault(String name, ChordType chordType, int position, Harmony harmony) {
        this.name = name;
        this.id = harmony.getName() + "." + name;
        this.chordType = chordType;
        this.position = position;
        this.stapes = chordType.getStapes().clone();
        for (int i = 0; i < stapes.length; i++) {
            stapes[i] += position;
        }
    }

    public String getName() {
        return name;
    }

    @Override
    public String getId() {
        return id;
    }

    public ChordType getType() {
        return chordType;
    }

    public int getPosition() {
        return position;
    }

    public double getFunction() {
        return function;
    }

    public void setFunction(double function) {
        if (function > 100 || function < 0) {
            throw new IllegalArgumentException("Achord function is % and can't be " + function);
        }
        this.function = function;
    }

    public int[] getStapes() {
        return stapes;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FunctionalChordDefault) {
            return id.equals(((FunctionalChordDefault)obj).getId());
        }
        return false;
    }

    @Override
    public int compareTo(FunctionalChord o) {
        int compare = Double.compare(function, o.getFunction());
        return compare == 0 ? id.compareTo(o.getId()) : compare;
    }

    @Override
    public String toString() {
        return id;
    }
}
