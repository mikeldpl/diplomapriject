package com.github.mikeldpl.ca.core.shell;

import com.github.mikeldpl.ca.core.Observer;

public interface Shell<T> extends Observer<T> {

    @Override
    default void notifyByObservable(T info) {
        fire(info);
    }

    void fire(T soundInfo);

    void close();
}
