package com.github.mikeldpl.ca.core.extractor;

import com.github.mikeldpl.ca.exception.CaException;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaMessage;
import java.nio.ByteBuffer;

public class MidiMetaMessageUtil {
    public static final byte NOTE_ON_MESSAGE = 85;
    public static final byte NOTE_OFF_MESSAGE = 86;
    public static final byte END_OF_TRACK_MESSAGE = 47;
    private static final int LENGTH_OF_MESSAGE = 9;

    private MidiMetaMessageUtil() {
    }

    private static byte[] generateNewMessage(byte key, long tiks) {
        byte[] res = new byte[LENGTH_OF_MESSAGE];
        res[0] = key;
        ByteBuffer.wrap(res, 1, 8).putLong(tiks);
        return res;
    }


    public static MetaMessage getMetaMessage(byte command, byte key, long tiks) {
        try {
            return new MetaMessage(command, generateNewMessage(key, tiks), LENGTH_OF_MESSAGE);
        } catch (InvalidMidiDataException e) {
            throw new CaException("Internal exception when press key on midi instrument.", e);
        }
    }

    public static byte getNote(MetaMessage meta) {
        return meta.getData()[0];
    }


    public static long getTicks(MetaMessage meta) {
        return ByteBuffer.wrap(meta.getData(), 1, 8).getLong();
    }

    public static int getType(MetaMessage meta) {
        return meta.getType();
    }
}
