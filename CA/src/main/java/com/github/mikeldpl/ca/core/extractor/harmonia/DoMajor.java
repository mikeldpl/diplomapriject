package com.github.mikeldpl.ca.core.extractor.harmonia;

import org.springframework.stereotype.Component;

@Component("DoMajor")
public class DoMajor implements Harmonia {
    private static int[] val = new int[]{0, 1, 2, 3, 0, 4, 3, 5, 1, 4, 5, 2};

    //                                0  1  2  3  4  5  6  7  8  9 10 11
    @Override
    public int[] get() {
        return val;
    }
}
