package com.github.mikeldpl.ca.core.extractor;

import com.github.mikeldpl.ca.exception.CaException;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.sound.midi.MetaMessage;

@Component("Poly Extractor For Instrument")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ExtractorMidiPolyTwoThread extends BaseExtractorPoly<MetaMessage> implements ExtractorPoly<MetaMessage>, ExtractorMidi {

    protected StarterOfCaProcess starterOfCaProcess;

    public ExtractorMidiPolyTwoThread() {
        this.starterOfCaProcess = buildStarterOfCaProcess();
        starterOfCaProcess.start();
    }

    protected StarterOfCaProcess buildStarterOfCaProcess() {
        return new StarterOfCaProcess();
    }

    @Override
    public void extract(MetaMessage meta) {
        if (meta.getType() == MidiMetaMessageUtil.NOTE_ON_MESSAGE) {
            starterOfCaProcess.nowElement(MidiMetaMessageUtil.getNote(meta));
        }
    }

    @Override
    public void close() {
        starterOfCaProcess.close();
    }


    protected class StarterOfCaProcess extends Thread {
        protected volatile long lastRealTime;

        public StarterOfCaProcess() {
            this("StarterOfCaProcess TwoThread");
        }

        protected StarterOfCaProcess(String name) {
            super("StarterOfCaProcess TwoThread");
        }

        @Override
        public synchronized void run() {
            try {
                long waitedTime;
                while (!isInterrupted()) {
                    if ((waitedTime = -System.currentTimeMillis() + getMaxDelayBetweenNotesThatPlayedTogether()
                            + lastRealTime) <= 0) {
                        draw();
                        wait();
                    } else {
                        wait(waitedTime);
                    }
                    notify();
                }
            } catch (InterruptedException e) {
                System.out.println("Interrupted StarterOfCaProcess thread");
            }
        }

        public synchronized void nowElement(byte note) {
            try {
                lastRealTime = System.currentTimeMillis();
                addNoteToMessage(note);
                notify();
                if (!isInterrupted()) {
                    wait();
                }
            } catch (InterruptedException e) {
                throw new CaException(e);
            }
        }

        public synchronized void close() {
            if (isAlive() && !isInterrupted()) {
                interrupt();
            }
        }
    }
}
