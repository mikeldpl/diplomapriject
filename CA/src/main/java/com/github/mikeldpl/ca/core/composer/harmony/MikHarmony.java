package com.github.mikeldpl.ca.core.composer.harmony;

import com.github.mikeldpl.ca.core.composer.harmony.chord.FunctionalChordDefault;
import com.github.mikeldpl.ca.core.composer.harmony.chord.ChordType;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Mixolydian")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MikHarmony extends HarmonyBase {

    private final FunctionalChordDefault T = new FunctionalChordDefault("T", ChordType.B53, getPositionByStage(0), this);
    private final FunctionalChordDefault M = new FunctionalChordDefault("M", ChordType.Dimin53, getPositionByStage(2), this);
    private final FunctionalChordDefault S = new FunctionalChordDefault("S", ChordType.B53, getPositionByStage(3), this);
    private final FunctionalChordDefault d = new FunctionalChordDefault("d", ChordType.M53, getPositionByStage(4), this);
    private final FunctionalChordDefault VII = new FunctionalChordDefault("VII", ChordType.B53, getPositionByStage(6), this);
    private final FunctionalChordDefault D7 = new FunctionalChordDefault("D7", ChordType.Cmin7, getPositionByStage(4), this);

    public MikHarmony() {
        super(0, TON, TON, HALF_TON, TON, TON, HALF_TON);
        this
                .addCords(T, S, D7, d, VII)
                .addCords(M, S, D7, M, d, VII)
                .addCords(S, T, M, VII)
                .addCords(d, T, M, VII)
                .addCords(VII, T, M, S, D7)
                .addCords(D7, VII, T);
    }

    @Override
    public String getName() {
        return "Mixolydian";
    }
}