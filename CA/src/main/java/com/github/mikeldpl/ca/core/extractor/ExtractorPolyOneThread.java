package com.github.mikeldpl.ca.core.extractor;

public interface ExtractorPolyOneThread<IN> extends Extractor<IN> {
    long MAX_DELAY_BETWEEN_NOTES_THAT_PLAYED_TOGETHER_DEFAULT_IN_TICKS = 60;

    long getMaxDelayBetweenNotesThatPlayedTogetherInTicks();

    void setMaxDelayBetweenNotesThatPlayedTogetherInTicks(long maxDelayBetweenNotesThatPlayedTogether);

}
