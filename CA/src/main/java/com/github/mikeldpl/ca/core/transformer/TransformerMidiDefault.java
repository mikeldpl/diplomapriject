package com.github.mikeldpl.ca.core.transformer;

import com.github.mikeldpl.ca.core.extractor.ExtractorMidi;
import org.springframework.stereotype.Component;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

@Component("TransformerMidiDefault")
public class TransformerMidiDefault extends TransformerBase<Sequence, Sequence, ExtractorMidi, TransformerStrategyMidi> implements TransformerMidi {
    @Override
    public void transform(Sequence in, Sequence out) {
        try {
            for (Track track : out.getTracks()) {
                transform(in.createTrack(), track);
            }
        } catch (InvalidMidiDataException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Class<TransformerStrategyMidi> getStrategyType() {
        return TransformerStrategyMidi.class;
    }

    private void transform(Track in, Track out) throws InvalidMidiDataException {
        for (int i = 0; i < out.size(); i++) {
            MidiEvent mEvent = out.get(i);
            MidiMessage midiMessage = (MidiMessage) mEvent.getMessage().clone();
            if (midiMessage instanceof ShortMessage) {
                ShortMessage shortMessage = (ShortMessage) midiMessage;
                if (shortMessage.getCommand() == ShortMessage.NOTE_ON || shortMessage.getCommand() == ShortMessage.NOTE_OFF) {
                    shortMessage.setMessage(shortMessage.getStatus(),
                            getTransformerStrategy().invert((byte) shortMessage.getData1(), shortMessage.getCommand(), mEvent.getTick(), getCa(), getExtractor()), shortMessage.getData2());
                }
            }
            in.add(new MidiEvent(midiMessage, mEvent.getTick()));
        }
    }


    @Override
    public Class<ExtractorMidi> getExtractorType() {
        return ExtractorMidi.class;
    }
}
