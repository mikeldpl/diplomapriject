package com.github.mikeldpl.ca.core.transformer;

import com.github.mikeldpl.ca.core.extractor.Extractor;
import com.github.mikeldpl.ca.core.src.BaseCaSource;

public abstract class TransformerBase<OUT, IN, EXT extends Extractor, STRATEGY extends TransformerStrategy>
        extends BaseCaSource<EXT> implements Transformer<OUT, IN, EXT, STRATEGY> {
    private STRATEGY strategy;

    @Override
    public STRATEGY getTransformerStrategy() {
        return strategy;
    }

    @Override
    public void setTransformerStrategy(STRATEGY transformerStratege) {
        this.strategy = transformerStratege;
    }
}
