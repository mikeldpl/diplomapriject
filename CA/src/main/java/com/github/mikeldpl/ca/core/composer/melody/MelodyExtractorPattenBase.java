package com.github.mikeldpl.ca.core.composer.melody;

import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;
import com.github.mikeldpl.ca.core.composer.melody.patterning.UsageChord;

public abstract class MelodyExtractorPattenBase extends MelodyExtractorBase {

    private int index;

    @Override
    public Melody extract(Chord chord, double temperament) {
        return getTemplateMelody(temperament, index++).use(chord);
    }

    protected abstract UsageChord getTemplateMelody(double temperament, int index);

    @Override
    public void close() {
        index = 0;
    }
}
