package com.github.mikeldpl.ca.core.ca;

import com.github.mikeldpl.ca.core.dst.drawer.Ca6Drawer;
import com.github.mikeldpl.ca.core.dst.drawer.CaDrawer;
import com.github.mikeldpl.ca.core.dst.interpreter.Ca6Interpreter;
import com.github.mikeldpl.ca.consts.CaConstants;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Iterator;


@Component("Ca6")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Ca6 extends CaBase<Ca6Area, CaDrawer<Ca6Area>> {
    private Ca6Area area = new Ca6Area(this);
    private int[] rules;

    public Ca6() {
        setLivingValues(CaConstants.LIVING_VALUES);
        setReviveValues(CaConstants.REVIVE_VALUES);
        setMaxState(CaConstants.MAX_STATE);
        rules = Arrays.copyOf(CaConstants.BEGIN_RULES, CaConstants.BEGIN_RULES.length);
    }

    @Override
    public void clear() {
        synchronized (getArea()) {
            rules = Arrays.copyOf(CaConstants.BEGIN_RULES, CaConstants.BEGIN_RULES.length);
            super.clear();
        }
    }

    @Override
    public Ca6Area getArea() {
        return area;
    }

    @Override
    public void setTestStates() {
        synchronized (getArea()) {
            clear();
            getArea().setTestState();
            notifyObservers();
        }
    }

    @Override
    public void process() {
//        long t = System.currentTimeMillis();
//        processRecursive();
        processLooped();
//        System.out.println(System.currentTimeMillis() - t);
    }

    private void processLooped() {
        int[] nextStates = new int[area.numberOfCells()];
        int i = 0;
        for (Cell6 curCell : area) {
            nextStates[i++] = getNextState(curCell);
        }
        i = 0;
        for (Cell6 curCell : area) {
            curCell.setState(nextStates[i++]);
        }
    }

    private void processRecursive() {
        pocRecStep(getArea().iterator());
    }

    private void pocRecStep(Iterator<Cell6> iterator) {
        if (iterator.hasNext()) {
            Cell6 curCell = iterator.next();
            int nextState = getNextState(curCell);
            pocRecStep(iterator);
            curCell.setState(nextState);
        }
    }

    private int getNextState(Cell6 cell) {
        byte numberOfNeighbors = 0;
        for (int i = 0; i < cell.getNeighbors().length; i++) {
            numberOfNeighbors += rules[i] == 0 ? 0 : cell.getNeighbors()[i].getState() > 0 ? 1 : 0;
        }
        return getState(cell.getState(), numberOfNeighbors);
    }

    @Override
    public Class<? extends Ca6Drawer> getDefaultDrawerClass() {
        return Ca6Drawer.class;
    }

    @Override
    public Class<? extends Ca6Interpreter> getDefaultInterpreterClass() {
        return Ca6Interpreter.class;
    }

    @Override
    public void setState(int[] sates) {
        synchronized (getArea()) {
            area.setState(sates);
            notifyObservers();
        }
    }

    @Override
    public int[] getState() {
        return area.getState();
    }

    @Override
    public void componRules(int[] rules) {
        for (int i = 0; i < rules.length; i++) {
            if (rules[i] > 0)
                this.rules[getHarmonia().get()[i]] ^= 1;
        }
//        System.out.println(Arrays.toString(this.rules));
//		for (Integer rule : rules) {
//			this.rules[CaConstants.NEIGHBORS_TO_NOTE[rule]] ^= 1;
//		}
    }

    public class Cell6 extends CaBase.CellBase {
        private int i;
        private int j;
        private Cell6[] neighbors = new Cell6[CaConstants.AREA.length];

        public Cell6(int i, int j) {
            this.i = i;
            this.j = j;
        }

        public int getI() {
            return i;
        }

        public int getJ() {
            return j;
        }

        public Cell6[] getNeighbors() {
            return neighbors;
        }

        @Override
        public boolean equals(Object o) {
            return o != null && getClass() == o.getClass() && i == ((Cell6) o).i && j == ((Cell6) o).j;
        }

        @Override
        public int hashCode() {
            return (i << 16) ^ j;
        }

        @Override
        public void setDefaultState() {
            setState(CaConstants.DEFAULT_CELL_STATE);
        }


        @Override
        public String toString() {
            return "[" + i + ", " + j + "] = " + getState();
        }
    }
}
