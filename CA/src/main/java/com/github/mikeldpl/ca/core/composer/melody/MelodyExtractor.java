package com.github.mikeldpl.ca.core.composer.melody;

import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;

public interface MelodyExtractor {
    void setTactDuration(int tact);
    Melody extract(Chord chord, double temperament);
    void close();
}
