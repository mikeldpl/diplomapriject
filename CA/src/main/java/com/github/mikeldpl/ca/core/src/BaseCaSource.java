package com.github.mikeldpl.ca.core.src;

import com.github.mikeldpl.ca.core.ca.Ca;
import com.github.mikeldpl.ca.core.extractor.Extractor;
import com.github.mikeldpl.ca.exception.CaException;

public abstract class BaseCaSource<EXT extends Extractor> implements CaSource<EXT> {
    private Ca ca;
    private EXT extractor;

    @Override
    public void close() {
        if (extractor != null)
            extractor.close();
    }

    @Override
    public EXT getExtractor() {
        return extractor;
    }

    @Override
    public void setExtractor(EXT ex) {
        if (ex == null)
            throw new CaException("Extractor  can't be null");
        if (extractor != null)
            extractor.close();
        extractor = ex;
        extractor.setCa(ca);
    }

    @Override
    public Ca getCa() {
        return ca;
    }

    @Override
    public void setCa(Ca ca) {
        this.ca = ca;
        if (extractor != null)
            extractor.setCa(ca);
    }
}
