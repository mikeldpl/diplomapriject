package com.github.mikeldpl.ca.core.composer;

public class CommandComposer {
    private double chord;
    private double temperament;
    private int move;

    public double getChord() {
        return chord;
    }

    public void setChord(double chord) {
        if (chord > 1 || chord < 0) {
            throw new IllegalArgumentException("" + chord);
        }
        this.chord = chord;
    }

    public double getTemperament() {
        return temperament;
    }

    public void setTemperament(double temperament) {
        if (temperament > 1 || temperament < 0) {
            throw new IllegalArgumentException();
        }
        this.temperament = temperament;
    }

    public int getMove() {
        return move;
    }

    public void setMove(int move) {
        this.move = move;
    }
}
