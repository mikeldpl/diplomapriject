package com.github.mikeldpl.ca.core.composer.harmony;

import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;

import java.util.Collection;

public interface PossibleChords {
    Chord getChord(double pos);
    Collection<PossibleChord> getPossibleChords();

    class PossibleChord {
        double pos;
        Chord chord;

        public PossibleChord(double pos, Chord chord) {
            this.pos = pos;
            this.chord = chord;
        }

        public double getPos() {
            return pos;
        }

        public Chord getChord() {
            return chord;
        }
    }
}
