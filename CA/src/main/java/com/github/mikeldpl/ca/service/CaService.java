package com.github.mikeldpl.ca.service;

import com.github.mikeldpl.ca.core.dst.drawer.CaDrawer;
import com.github.mikeldpl.ca.consts.CaConstants;
import com.github.mikeldpl.ca.core.ca.Ca;
import com.github.mikeldpl.ca.core.extractor.harmonia.Harmonia;
import com.github.mikeldpl.ca.exception.CaException;
import javafx.scene.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.stream.Stream;

@Service
public class CaService implements ContainerService<Ca> {
    @Autowired
    private ApplicationContext context;
    @Autowired
    private InstrumentService instrumentService;
    @Autowired
    private InterpreterService interpreterService;
    @Autowired
    private ConfigService configService;
    private Ca ca;
    private Group drawArea;
    private CaDrawer defaultDrawer;

    public void setSize(int i, int size) {
        ca.setSize(i, size);
    }

    public Ca getCa() {
        return ca;
    }

    public void setCa(Ca ca) {
        defaultDrawer = context.getBean(CaConstants.DRAWER, (Class<CaDrawer>) ca.getDefaultDrawerClass());
        defaultDrawer.setDrawArea(drawArea);
        ca.addObserver(defaultDrawer);
        ca.notifyObservers();
        this.ca = ca;
    }

    public void setCa(String caName) {
        setCa(context.getBean(caName, Ca.class));
        instrumentService.setCa(ca);
        interpreterService.addCaListeners(ca);
    }

    public void setState() {
        ca.setState(configService.getCaState());
    }

    public void setDrawArea(Group drawArea) {
        this.drawArea = drawArea;
        if (ca != null) {
            for (CaDrawer caDrawer : (Iterable<CaDrawer>) ca.getObservers()) {
                caDrawer.setDrawArea(drawArea);
            }
        }
    }

    public void setSize(int... sizes) {
        if (sizes.length > ca.getNumberSizes())
            throw new CaException("Size Ca = " + ca.getNumberSizes() + " not " + sizes.length);
        for (int i = 0; i < sizes.length; i++) {
            ca.setSize(i, sizes[i]);
        }
    }

    public void setLivingDiapason(int from, int to) {
        ca.setLivingDiapason(from, to);
    }

    public void setReviveDiapason(int from, int to) {
        ca.setReviveDiapason(from, to);
    }

    public void setRandomBeginStates() {
        ca.setRandomBeginStates();
    }

    public void clear() {
        ca.clear();
    }

    public void setMaxState(int maxState) {
        ca.setMaxState(maxState);
    }

    public void setTestState() {
        ca.setTestStates();
    }

    public CaDrawer getDrawer() {
        return defaultDrawer;
    }

    public void setHarmonia(String name) {
        ca.setHarmonia(context.getBean(name, Harmonia.class));
    }

    @PreDestroy
    public void close() {
        configService.setCaState(ca.getState());
    }

    @Override
    public Stream<Ca> getContent() {
        return Stream.of(ca);
    }
}
