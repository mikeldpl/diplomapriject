package com.github.mikeldpl.ca.core.src.player;

import com.github.mikeldpl.ca.core.extractor.ExtractorMidi;
import com.github.mikeldpl.ca.core.extractor.ExtractorPoly;
import com.github.mikeldpl.ca.core.extractor.ExtractorPolyOneThread;
import com.github.mikeldpl.ca.exception.CaException;
import com.github.mikeldpl.ca.core.extractor.MidiMetaMessageUtil;
import com.github.mikeldpl.ca.core.transformer.TransformerMidi;
import javafx.stage.FileChooser;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;
import java.io.File;
import java.io.IOException;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CaPlayerMidi extends CaPlayerBase<ExtractorMidi, TransformerMidi> {
    public static final long MAX_DELAY_BETWEEN_NOTES_THAT_PLAYED_TOGETHER_DEFAULT = 200;
    public static final long MAX_DELAY_BETWEEN_NOTES_THAT_PLAYED_TOGETHER_DEFAULT_IN_TICKS = 8;
    private static final FileChooser.ExtensionFilter EXTENSION_FILTER_FILTERS = new FileChooser.ExtensionFilter("MIDI", "*.midi", "*.mid");
    private Sequencer sequencer;
    private boolean lastMute;
    private CaSequence caSequenceHolder;

    public CaPlayerMidi() {
        try {
            sequencer = MidiSystem.getSequencer();
            sequencer.open();
            sequencer.addMetaEventListener(meta -> {
                try {
                    getExtractor().extract(meta);
                    if (meta.getType() == MidiMetaMessageUtil.END_OF_TRACK_MESSAGE) {
                        System.out.println("end of midi");
                        stop();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        } catch (MidiUnavailableException e) {
            throw new CaException(e);
        }
    }

    @Override
    public double getProgress() {
        return sequencer.getTickPosition() / (double) sequencer.getTickLength();
    }

    @Override
    public FileChooser.ExtensionFilter getExtensionFilterFilter() {
        return EXTENSION_FILTER_FILTERS;
    }

    private MetaMessage transformMessage(ShortMessage shortMessage, byte newType, long tiks) throws InvalidMidiDataException {
        return MidiMetaMessageUtil.getMetaMessage(newType, shortMessage.getMessage()[1], tiks);
    }

    @Override
    public void play(File path) {
        try {
            Sequence sequence = getSequence(path);
            caSequenceHolder = new CaSequence(sequence);
            sequencer.setSequence(caSequenceHolder.getSequence());
            mute(lastMute);
            sequencer.start();
            super.play(path);
        } catch (InvalidMidiDataException | IOException e) {
            throw new CaException(e);
        }
    }

    private Sequence getSequence(File path) throws InvalidMidiDataException, IOException {
        Sequence sequence = MidiSystem.getSequence(path);
        if (isTransformed()) {
            Sequence newSequence = new Sequence(sequence.getDivisionType(), sequence.getResolution());
            getTransformer().setExtractor(getExtractor());
            getTransformer().setCa(getCa());
            getTransformer().transform(newSequence, sequence);
            sequence = newSequence;
        }
        return sequence;
    }

    @Override
    public void stop() {
        super.stop();
        sequencer.stop();
        caSequenceHolder = null;
        if (getExtractor() != null)
            getExtractor().extract(MidiMetaMessageUtil.getMetaMessage(MidiMetaMessageUtil.END_OF_TRACK_MESSAGE, (byte) 0, sequencer.getTickPosition()));
        System.out.println("stop midi");
    }

    @Override
    public Class<TransformerMidi> getTransformerType() {
        return TransformerMidi.class;
    }

    @Override
    public void close() {
        super.close();
        sequencer.close();
    }

    @Override
    public void setExtractor(ExtractorMidi ex) {
        super.setExtractor(ex);
        if (ex instanceof ExtractorPolyOneThread) {
            ((ExtractorPolyOneThread) ex).setMaxDelayBetweenNotesThatPlayedTogetherInTicks(MAX_DELAY_BETWEEN_NOTES_THAT_PLAYED_TOGETHER_DEFAULT_IN_TICKS);
            if (ex instanceof ExtractorPoly) {
                ((ExtractorPoly) ex).setMaxDelayBetweenNotesThatPlayedTogether(MAX_DELAY_BETWEEN_NOTES_THAT_PLAYED_TOGETHER_DEFAULT);
            }
        }
    }

    @Override
    public Class<ExtractorMidi> getExtractorType() {
        return ExtractorMidi.class;
    }

    @Override
    public void mute(boolean isMuted) {
        lastMute = isMuted;
        if (caSequenceHolder != null) {
            for (int i = 0; i < caSequenceHolder.getNeuterOfRealTeaks(); i++) {
                sequencer.setTrackMute(i, isMuted);
            }
        }
    }

    private class CaSequence {
        private Sequence sequence;
        private int neuterOfRealTeaks;

        public CaSequence(Sequence sequence) throws InvalidMidiDataException {
            neuterOfRealTeaks = sequence.getTracks().length;
            addMetaTrackToAllTracks(sequence);
            this.sequence = sequence;
        }

        public Sequence getSequence() {
            return sequence;
        }

        private void addMetaTrackToAllTracks(Sequence sequence) throws InvalidMidiDataException {
            Track[] tracks = sequence.getTracks();
            for (Track track : tracks) {
                addNotesToTrack(track, sequence.createTrack());
            }
        }

        private void addNotesToTrack(Track track, Track newMetaTrack) throws InvalidMidiDataException {
            for (int i = 0; i < track.size(); i++) {
                MidiEvent mEvent = track.get(i);
                if (mEvent.getMessage() instanceof ShortMessage) {
                    ShortMessage shortMessage = (ShortMessage) mEvent.getMessage();
                    switch (shortMessage.getCommand()) {
                        case ShortMessage.NOTE_ON:
                            newMetaTrack.add(new MidiEvent(transformMessage(shortMessage, MidiMetaMessageUtil.NOTE_ON_MESSAGE, mEvent.getTick()),
                                    mEvent.getTick()));
                            break;
                        case ShortMessage.NOTE_OFF:
                            newMetaTrack.add(new MidiEvent(transformMessage(shortMessage, MidiMetaMessageUtil.NOTE_OFF_MESSAGE, mEvent.getTick()),
                                    mEvent.getTick()));
                            break;
                    }
                }
            }
        }

        public int getNeuterOfRealTeaks() {
            return neuterOfRealTeaks;
        }
    }
}
