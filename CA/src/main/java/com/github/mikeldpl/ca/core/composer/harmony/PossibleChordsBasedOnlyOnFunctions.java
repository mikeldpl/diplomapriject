package com.github.mikeldpl.ca.core.composer.harmony;


import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;
import com.github.mikeldpl.ca.core.composer.harmony.chord.FunctionalChord;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PossibleChordsBasedOnlyOnFunctions implements PossibleChords {
    private List<PossibleChord> functionalChords;

    public PossibleChordsBasedOnlyOnFunctions(Set<FunctionalChord> functionalChords) {
        double sum = functionalChords.stream().mapToDouble(FunctionalChord::getFunction).sum();
        this.functionalChords = functionalChords.stream()
                .filter(functionalChord -> functionalChord.getFunction() > 0)
                .sorted(Comparator.reverseOrder())
                .map(functionalChord -> new PossibleChord(functionalChord.getFunction()/sum, functionalChord))
                .collect(Collectors.toList());
    }

    @Override
    public Chord getChord(double position) {
        if (functionalChords.size() == 0) {
            return null;
        }
        double curPos = 0;
        for (PossibleChord possibleChord : functionalChords) {
            curPos += possibleChord.pos;
            if (curPos >= position)
                return possibleChord.chord;
        }
        throw new IllegalArgumentException("Position should be less then " + curPos);
    }

    @Override
    public Collection<PossibleChord> getPossibleChords() {
        return functionalChords;
    }
}
