package com.github.mikeldpl.ca.core.extractor;

import com.github.mikeldpl.ca.core.ca.Ca;


public interface Extractor<IN> {
    void extract(IN message);

    Ca getCa();

    void setCa(Ca ca);

    void close();
}
