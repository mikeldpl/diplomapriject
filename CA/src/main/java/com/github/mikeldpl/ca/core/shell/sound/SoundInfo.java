package com.github.mikeldpl.ca.core.shell.sound;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class SoundInfo implements Iterable<NoteAction> {

    private Collection<NoteAction> notes = new ArrayList<>(3);

    private SoundInfo() {
    }

    public static SoundInfo inst() {
        return new SoundInfo();
    }

    public SoundInfo addNoteAction(NoteAction action) {
        notes.add(action);
        return this;
    }

    public SoundInfo onOffNote(byte key, long duration) {
        return addNoteAction(NoteAction.instNoteOn(key))
                .addNoteAction(NoteAction.instNoteOff(key, duration));
    }

    public SoundInfo onOffNote(byte key, long delay, long duration) {
        return addNoteAction(NoteAction.instNoteOn(key, delay))
                .addNoteAction(NoteAction.instNoteOff(key, delay + duration));
    }

    @Override
    public Iterator<NoteAction> iterator() {
        return notes.iterator();
    }

    public SoundInfo notesOn(byte key) {
        return addNoteAction(NoteAction.instNoteOn(key));
    }

    public SoundInfo notesOff(byte key) {
        return addNoteAction(NoteAction.instNoteOff(key));
    }
}
