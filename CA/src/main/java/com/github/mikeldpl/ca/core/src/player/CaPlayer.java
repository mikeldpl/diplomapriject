package com.github.mikeldpl.ca.core.src.player;

import com.github.mikeldpl.ca.core.extractor.Extractor;
import com.github.mikeldpl.ca.core.sound.SoundAwareBase;
import com.github.mikeldpl.ca.core.sound.SoundSource;
import com.github.mikeldpl.ca.core.src.CaSource;
import com.github.mikeldpl.ca.core.transformer.Transformer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.stage.FileChooser;

import java.io.File;

public interface CaPlayer<EXT extends Extractor, TRANSFORMER extends Transformer> extends CaSource<EXT>,
        SoundAwareBase {
    FileChooser.ExtensionFilter getExtensionFilterFilter();

    void play(File path);

    void stop();

    DoubleProperty getProgressProperty();

    void setProgressProperty(DoubleProperty progressProperty);

    BooleanProperty getStoppedProperty();

    void setStoppedProperty(BooleanProperty stoppedProperty);

    void setIsTransformed(boolean transformed);

    boolean isTransformed();

    TRANSFORMER getTransformer();

    void setTransformer(TRANSFORMER transformer);

    Class<TRANSFORMER> getTransformerType();

    @Override
    default SoundSource getSoundSource() {
        return SoundSource.SOURCE;
    }
}
