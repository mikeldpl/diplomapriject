package com.github.mikeldpl.ca.core.composer.melody.patterning;

import com.github.mikeldpl.ca.consts.MusicConstants;
import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;

import java.util.Arrays;

@FunctionalInterface
public interface ChordMask {

    static ChordMask pause() {
        return chord -> new int[0];
    }

    static ChordMask first(int n) {
        return chord -> Arrays.copyOf(chord.getStapes(), n);
    }

    static ChordMask powerUp(int n) {
        return chord -> {
            int[] ints = Arrays.copyOf(chord.getStapes(),
                    chord.getStapes().length + n);
            for (int i = 0; i < n; i++) {
                ints[chord.getStapes().length + i] = chord.getStapes()[i]
                        + MusicConstants.NOTES_LENGTH;
            }
            return ints;
        };
    }

    int[] mask(Chord chord);
}
