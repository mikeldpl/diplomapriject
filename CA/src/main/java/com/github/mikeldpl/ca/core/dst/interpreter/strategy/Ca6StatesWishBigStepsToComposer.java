package com.github.mikeldpl.ca.core.dst.interpreter.strategy;

import com.github.mikeldpl.ca.consts.ComposerConstants;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("State with movement")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Ca6StatesWishBigStepsToComposer extends Ca6StatesToComposer {

    @Override
    protected int getMove(Ca6StatesToComposer.StateAreaInfo stateInfo) {
        if (prevStateInfo == null) {
            return 0;
        }
        return (int) Math.round(ComposerConstants.DIAPASON_NOTE *
                (stateInfo.liveCells - prevStateInfo.liveCells)
                / (double)prevStateInfo.maxCells);
    }
}
