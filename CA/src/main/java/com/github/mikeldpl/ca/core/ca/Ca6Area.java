package com.github.mikeldpl.ca.core.ca;

import com.github.mikeldpl.ca.consts.CaConstants;
import com.github.mikeldpl.ca.exception.CaException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;


public class Ca6Area implements CaArea<Ca6.Cell6> {

    private HashMap<Ca6.Cell6, Ca6.Cell6> cells = new HashMap<>();
    private int[] sizes = new int[getNumberOfSizes()];
    private Ca6 ca;
    private int a;
    private int b;
    private int c;

    public Ca6Area(Ca6 ca6) {
        this.ca = ca6;
        for (int i = 0; i < sizes.length; i++) {
            sizes[i] = getMinSize();
        }
        refreshSizes();
    }

    @Override
    public int[] getSizes() {
        return sizes;
    }

    @Override
    public int numberOfCells() {
        return cells.size();
    }

    @Override
    public synchronized void setRandomBeginStates() {
        Random random = new Random(System.currentTimeMillis());
        for (Ca6.Cell6 cell6 : this) {
            cell6.setState(Math.abs(random.nextDouble() > CaConstants.NON_ZERO_STATE_RANDOM ? 0 : (random.nextInt() % cell6.getMaxState() + 1)));
        }
    }

    @Override
    public int getDimension() {
        return CaConstants.DIMENSION;
    }

    @Override
    public int getNumberOfSizes() {
        return CaConstants.NUMBER_SIZES;
    }

    @Override
    public int getMinSize() {
        return CaConstants.MIN_SIZE;
    }

    @Override
    public synchronized Ca6.Cell6 getCell(int... index) {
        if (index.length != getDimension()) {
            throw new CaException("dimension = " + getDimension() + " and you set size with index " + Arrays.toString(index));
        }
        return getCell(index[0], index[1]);
    }

    private Ca6.Cell6 getCell(int i, int j) {
        return cells.get(getNewCellWithCorrectCords(i, j));
    }

    @Override
    public synchronized void setSize(int i, int value) {
        if (i < 0 || i >= getNumberOfSizes())
            throw new CaException("index can't less 0 and be more then " + (getNumberOfSizes() - 1));
        if (value < getMinSize())
            throw new CaException("size of CA can't be less then " + getMinSize());
        if (sizes[i] != value) {
            sizes[i] = value;
            refreshSizes();
        }
    }

    @Override
    public synchronized void clear() {
        for (Cell cell : this) {
            cell.setDefaultState();
        }
    }

    private Ca6.Cell6 getNewCellWithCorrectCords(int i, int j) {
        boolean flag;
        do {
            flag = false;
            if (i < 0) {//must use while
                j -= c;
                i += b;
                flag = true;
            } else if (i >= b) {
                j += c;
                i -= b;
                flag = true;
            }
            if (j < 0) {
                j += a;
                i -= c;
                flag = true;
            } else if (j >= a) {
                j -= a;
                i += c;
                flag = true;
            }
            if (i < c - j) {
                j += a - c;
                i += b - c;
                flag = true;
            } else if (!isValidIJByRightSide(i, j)) {
                j -= a - c;
                i -= b - c;
                flag = true;
            }
        } while (flag);
        return ca.new Cell6(i, j);
    }

    private Ca6.Cell6 getCreateNew(int i, int j) {
        Ca6.Cell6 newCell = getNewCellWithCorrectCords(i, j), res = cells.get(newCell);
        if (res == null) {
            cells.put(newCell, newCell);
            return newCell;
        }
        return res;
    }

    private void refreshSizes() {
        initABC();
        cells.clear();
        for (int i = 0; i < b; i++) {
            for (int j = getJIndex(i); j < a && isValidIJByRightSide(i, j); j += 3) {
                Ca6.Cell6 cell6 = getCreateNew(i, j);
                for (int k = 0; k < cell6.getNeighbors().length; k++) {
                    cell6.getNeighbors()[k] = getCreateNew(cell6.getI() + CaConstants.AREA[k][0], cell6.getJ() + CaConstants.AREA[k][1]);
                    cell6.getNeighbors()[k].getNeighbors()[(k + 3) % 6] = cell6;
                }
            }
        }
    }

    @Override
    public synchronized void setTestState() {
        for (Ca6.Cell6 cell6 : this) {
            if (cell6.getJ() > a * (1 - CaConstants.TEST_OFFSET) && cell6.getJ() < a * CaConstants.TEST_OFFSET &&
                    cell6.getI() > b * (1 - CaConstants.TEST_OFFSET) && cell6.getI() < b * CaConstants.TEST_OFFSET
                    && cell6.getI() + cell6.getJ() > c + (b + a - c - c) * (1 - CaConstants.TEST_OFFSET)
                    && cell6.getI() + cell6.getJ() < a + b - c - (b + a - c - c) * (1 - CaConstants.TEST_OFFSET))
                cell6.setState(1);
        }
    }

    @Override
    public synchronized void setState(int[] states) {
        if (states != null) {
            if (states.length != numberOfCells()) {
                throw new CaException("Invalid states count " + cells.size() + " != " + states.length);
            }
            Iterator<Ca6.Cell6> iterator = iterator();
            for (int i = 0; i < states.length; i++) {
                iterator.next().setState(states[i]);
            }
        }
    }

    @Override
    public synchronized int[] getState() {
        return cells.keySet().stream().mapToInt(CaBase.CellBase::getState).toArray();
    }

    private void initABC() {
        a = (sizes[0] + sizes[2] - 2) * 3 + 2;
        b = (sizes[1] + sizes[2] - 2) * 3 + 2;
        c = (sizes[2] - 1) * 3 + 1;
    }


    private int getJIndex(int i) {
        int j = c - i - 1 > 0 ? c - 1 - i : 0;
        j += i % 3 - j % 3 + (i % 3 < j % 3 ? 3 : 0);
        return j;
    }

    private boolean isValidIJByRightSide(int i, int j) {
        return j < a + b - c - i;
    }

    @Override
    public Iterator<Ca6.Cell6> iterator() {
        return cells.keySet().iterator();
    }
}
