package com.github.mikeldpl.ca.core.composer.melody.patterning;

import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;
import com.github.mikeldpl.ca.core.composer.melody.Melody;

public class UsageChord {
    private Press[] presses;

    public UsageChord(Press... presses) {
        this.presses = presses;
    }

    public Melody use(Chord chord) {
        Melody melody = new Melody();
        for (Press press : presses) {
            for (int note : press.chordMask.mask(chord)) {
                melody.addInThisMoment(note, press.duration);
            }
            melody.shiftMoment(press.duration);
        }
        return melody;
    }
}
