package com.github.mikeldpl.ca.core.sound;

public interface SoundAware extends Mutable {
    void acceptSoundSource(SoundSource soundSource);
}
