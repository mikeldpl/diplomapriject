package com.github.mikeldpl.ca.core.shell.sound;

import com.github.mikeldpl.ca.exception.CaException;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.IntConsumer;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AudioMidiShell extends BaseSoundShell {
    //    private static final AtomicInteger nextChannel = new AtomicInteger(0);
    private static final byte VELOCITY_DEFAULT = 59;

    private MidiChannel channel;
    private Synthesizer synth;
    private Instrument[] instruments;
    private byte velocity;
    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();


    public AudioMidiShell() {
        try {
            synth = MidiSystem.getSynthesizer();
            synth.open();
            instruments = synth.getAvailableInstruments();
            channel = synth.getChannels()[0];
            setVelocity(VELOCITY_DEFAULT);
        } catch (MidiUnavailableException e) {
            throw new CaException(e);
        }
    }

    public byte getVelocity() {
        return velocity;
    }

    public void setVelocity(byte velocity) {
        this.velocity = velocity;
    }

    public Instrument[] getInstruments() {
        return instruments;
    }

    @Override
    public void fire(SoundInfo soundInfo, IntConsumer preProcess) {
        for (NoteAction noteAction : soundInfo) {
            if (noteAction.delay == 0) {
                consumeNoteAction(noteAction, preProcess);
            } else {
                executorService.schedule(() -> consumeNoteAction(noteAction, preProcess),
                        noteAction.delay, TimeUnit.MILLISECONDS);
            }
        }
    }

    @Override
    protected void consumeOn(byte key) {
        channel.noteOn(key, velocity);
    }

    @Override
    protected void consumeOff(byte key) {
        channel.noteOff(key, velocity);
    }

    @Override
    public void setInstrumentVoice(String instrument) {
        for (Instrument instrument1 : instruments) {
            if (instrument1.getName().toLowerCase().trim().equals(instrument.toLowerCase().trim())) {
                channel.programChange(instrument1.getPatch().getProgram(), instrument1.getPatch().getBank());
                return;
            }
        }
        System.err.println("Invalid instrument " + instrument);
    }

    @Override
    public String[] getInstrumentVoices() {
        String[] names = new String[instruments.length];
        for (int i = 0; i < instruments.length; i++) {
            names[i] = instruments[i].getName();
        }
        return names;
    }

    @Override
    public void close() {
        synth.close();
        if (!executorService.isShutdown()) {
            executorService.shutdownNow();
        }
    }

    @Override
    public void mute(boolean isMuted) {
        channel.setMute(isMuted);
    }
}
