package com.github.mikeldpl.ca.core.composer.harmony;

import com.github.mikeldpl.ca.core.composer.harmony.chord.FunctionalChordDefault;
import com.github.mikeldpl.ca.core.composer.harmony.chord.ChordType;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Major")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MajHarmony extends HarmonyBase {

    private final FunctionalChordDefault T = new FunctionalChordDefault("T", ChordType.B53, getPositionByStage(0), this);
    private final FunctionalChordDefault II = new FunctionalChordDefault("II", ChordType.M53, getPositionByStage(1), this);
    private final FunctionalChordDefault IId = new FunctionalChordDefault("II#", ChordType.B53, getPositionByStage(1), this);
    private final FunctionalChordDefault M = new FunctionalChordDefault("M", ChordType.B53, getPositionByStage(2), this);
    private final FunctionalChordDefault m = new FunctionalChordDefault("m", ChordType.M53, getPositionByStage(2), this);
    private final FunctionalChordDefault S = new FunctionalChordDefault("S", ChordType.B53, getPositionByStage(3), this);
    private final FunctionalChordDefault s = new FunctionalChordDefault("s", ChordType.M53, getPositionByStage(3), this);
    private final FunctionalChordDefault D = new FunctionalChordDefault("D", ChordType.B53, getPositionByStage(4), this);
    private final FunctionalChordDefault d = new FunctionalChordDefault("d", ChordType.M53, getPositionByStage(4), this);
    private final FunctionalChordDefault VI = new FunctionalChordDefault("VI", ChordType.M53, getPositionByStage(5), this);
    private final FunctionalChordDefault VId = new FunctionalChordDefault("VI#", ChordType.B53, getPositionByStage(5), this);
    private final FunctionalChordDefault VIb = new FunctionalChordDefault("VIb", ChordType.B53, getPositionByStage(5) - 1, this);
    private final FunctionalChordDefault VII = new FunctionalChordDefault("VII", ChordType.Dimin53, getPositionByStage(6), this);
    private final FunctionalChordDefault VIIb = new FunctionalChordDefault("VIIb", ChordType.B53, getPositionByStage(6) - 1, this);
    private final FunctionalChordDefault D7 = new FunctionalChordDefault("D7", ChordType.D7, getPositionByStage(4), this);
    private final FunctionalChordDefault M57d = new FunctionalChordDefault("M-57#", ChordType.CmM7, getPositionByStage(6), this);
    private final FunctionalChordDefault M57 = new FunctionalChordDefault("M-57", ChordType.Cdim7, getPositionByStage(6), this);
    private final FunctionalChordDefault II7 = new FunctionalChordDefault("II7", ChordType.Cmin7, getPositionByStage(1), this);

    public MajHarmony() {
        super(0, TON, TON, HALF_TON, TON, TON, TON);
        this
                .addCords(T, M, m, S, D, s, d, VI, VII, D7, M57d, M57, II7)
                .addCords(II, S, s, VI, VId, VIIb, m, D, d)
                .addCords(IId, D, d, M, IId, VId)
                .addCords(M, T, IId, VI, S)
                .addCords(m, T, II, D, VI, S)
                .addCords(S, T, II, m, M, D, d, VI, VIIb)
                .addCords(s, T, II, D, d, VIb, VIIb)
                .addCords(D, T, II, IId, m, S, s, VI, VIIb, VII, M57d, M57, II7, D7)
                .addCords(d, T, II, IId, S, s, VId, VIb, VII)
                .addCords(VI, T, II, m, M, VII, S, D)
                .addCords(VId, II, IId, d)
                .addCords(VIb, s, d)
                .addCords(VII, T, VI, D, D7, M57d, M57, II7)
                .addCords(VIIb, II, s, S)

                .addCords(D7, T, VII, D)
                .addCords(M57d, T, VII, D)
                .addCords(M57, T, VII, D)
                .addCords(II7, T, VII, D);

    }

    @Override
    public String getName() {
        return "Major";
    }
}
