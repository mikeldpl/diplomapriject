package com.github.mikeldpl.ca.core.shell.sound;

public abstract class SoundSellMonitor extends BaseSoundShell {
    @Override
    public void mute(boolean isMuted) {
    }

    @Override
    public void setInstrumentVoice(String instrument) {
    }

    @Override
    public String[] getInstrumentVoices() {
        return new String[0];
    }

    @Override
    public void close() {
    }
}
