package com.github.mikeldpl.ca.core.composer;

import com.github.mikeldpl.ca.core.composer.harmony.Harmony;
import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;
import com.github.mikeldpl.ca.core.composer.harmony.chord.MovableChord;
import com.github.mikeldpl.ca.core.composer.melody.Melody;
import com.github.mikeldpl.ca.core.composer.melody.MelodyExtractor;
import com.github.mikeldpl.ca.core.composer.melody.accompaniment.Accompaniment;
import com.github.mikeldpl.ca.core.shell.sound.SoundInfo;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;

import static com.github.mikeldpl.ca.consts.ComposerConstants.MAX_NOTE;
import static com.github.mikeldpl.ca.consts.ComposerConstants.MIN_NOTE;

@Component("Simple Composer")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ComposerImpl implements Composer {

    public static final boolean TRACE = true;
    private int tact;
    private long duration;
    private MelodyExtractor melodyExtractor;
    private Note stage;
    private Harmony harmony;

    private long lastPlayedTime;
    private int position;
    private Chord lastChord;
    private Accompaniment accompaniment;

    public ComposerImpl() {
        clean();
    }

    @Override
    public SoundInfo compose(CommandComposer commandComposer) {
        if (commandComposer != null && (lastChord = getChord(commandComposer)) != null) {
            if (TRACE) {
                System.out.println(lastChord);
            }
            Chord chord = moveChord(commandComposer.getMove());
            Melody melody = melodyExtractor.extract(chord, commandComposer.getTemperament());
            accompaniment.accompany(melody, chord, commandComposer.getTemperament());
            return convertToSoundInfo(melody);
        }
        clean();
        return null;
    }

    private Chord getChord(CommandComposer commandComposer) {
        if (commandComposer.getChord() == 0 && lastChord != null) {
            return lastChord;
        }
        return harmony
                .getPossibleHarmoniousChords(lastChord)
                .getChord(commandComposer.getChord());
    }

    private SoundInfo convertToSoundInfo(Melody melody) {
        Collection<Melody.Note> notes = melody.getNotes();
        SoundInfo noteActions = SoundInfo.inst();
        long offset = lastPlayedTime - System.currentTimeMillis();
        offset = offset > 0 ? offset : 0;
        long durationOfNoteTick = duration / Melody.ONE_4;
        for (Melody.Note note : notes) {
            noteActions.onOffNote((byte) note.value,
                    note.moment * durationOfNoteTick + offset,
                    note.duration * durationOfNoteTick);
        }
        lastPlayedTime = System.currentTimeMillis() + offset
                + durationOfNoteTick * melody.getCommonDuration();
        return noteActions;
    }

    private Chord moveChord(int move) {
        MovableChord chord = MovableChord.fromBaseChordAndStage(lastChord, stage);
        position += move;
        if (position > MAX_NOTE) {
            position = MAX_NOTE;
        } else if (position < MIN_NOTE) {
            position = MIN_NOTE;
        }
        if (move < 0) {
            chord.moveLess(position);
        } else {
            chord.moveHigher(position);
        }
        return chord;
    }

    public void clean() {
        position = (MAX_NOTE + MIN_NOTE) / 2;
        lastChord = null;
        if (melodyExtractor != null) {
            melodyExtractor.close();
        }
    }

    @Override
    public void setTact(int tact) {
        this.tact = tact;
    }

    @Override
    public void setDuration(long duration) {
        this.duration = duration;
    }

    @Override
    public void setMelodyExtractor(MelodyExtractor melodyExtractor) {
        this.melodyExtractor = melodyExtractor;
        if (this.melodyExtractor != null) {
            this.melodyExtractor.setTactDuration(tact * Melody.ONE_8);
        }
    }

    @Override
    public void setAccompaniment(Accompaniment accompaniment) {
        this.accompaniment = accompaniment;
    }

    @Override
    public void setStage(Note stage) {
        this.stage = stage;
    }

    @Override
    public void setHarmony(Harmony harmony) {
        this.harmony = harmony;
    }
}
