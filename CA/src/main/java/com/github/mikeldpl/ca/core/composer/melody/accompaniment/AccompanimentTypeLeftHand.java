package com.github.mikeldpl.ca.core.composer.melody.accompaniment;

import com.github.mikeldpl.ca.core.composer.harmony.chord.MovableChord;
import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Default AccompanimentType")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AccompanimentTypeLeftHand implements AccompanimentType {
    @Override
    public Chord shiftChord(Chord baseChord) {
        return MovableChord.octaveDown(baseChord, 1);
    }
}
