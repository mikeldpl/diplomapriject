package com.github.mikeldpl.ca.consts;

public class ComposerConstants {

    public static final int MAX_NOTE = MusicConstants.FIRST_OCTAVE + 12*2;
    public static final int MIN_NOTE = MusicConstants.FIRST_OCTAVE - 12*2;
    public static final int DIAPASON_NOTE = MAX_NOTE - MIN_NOTE;
    private ComposerConstants() {
    }
}
