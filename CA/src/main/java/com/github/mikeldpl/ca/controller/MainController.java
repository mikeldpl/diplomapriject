package com.github.mikeldpl.ca.controller;

import com.github.mikeldpl.ca.consts.BaseConstants;
import com.github.mikeldpl.ca.core.extractor.Extractor;
import com.github.mikeldpl.ca.core.extractor.harmonia.Harmonia;
import com.github.mikeldpl.ca.core.sound.SoundSource;
import com.github.mikeldpl.ca.service.CaService;
import com.github.mikeldpl.ca.service.ConfigService;
import com.github.mikeldpl.ca.service.InterpreterService;
import com.github.mikeldpl.ca.core.dst.interpreter.strategy.CaInterpreterStrategy;
import com.github.mikeldpl.ca.service.InstrumentService;
import com.github.mikeldpl.ca.service.PlayerService;
import com.github.mikeldpl.ca.service.SoundManagerService;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;

import java.io.File;

@Controller
public class MainController {

    @FXML
    public Button start_button;
    @FXML
    public Button stop_button;
    @FXML
    public Group drawArea;
    @FXML
    public Slider size_slider0;
    @FXML
    public Slider size_slider1;
    @FXML
    public Slider size_slider2;
    @FXML
    public Button choseFile_button;
    @FXML
    public ProgressBar progress;
    @FXML
    public Label choseFile_field;
    @FXML
    public GridPane root;
    @FXML
    public TextField livingValue1_test;
    @FXML
    public TextField reviveValue1_test;
    @FXML
    public TextField livingValue2_test;
    @FXML
    public TextField reviveValue2_test;
    @FXML
    public Button random_button;
    @FXML
    public Button clear_button;
    @FXML
    public Button test_button;
    @FXML
    public Slider max_states;
    @FXML
    public Rectangle note$0;
    @FXML
    public Rectangle note$2;
    @FXML
    public Rectangle note$4;
    @FXML
    public Rectangle note$5;
    @FXML
    public Rectangle note$7;
    @FXML
    public Rectangle note$9;
    @FXML
    public Rectangle note$11;
    @FXML
    public Rectangle note$1;
    @FXML
    public Rectangle note$3;
    @FXML
    public Rectangle note$6;
    @FXML
    public Rectangle note$8;
    @FXML
    public Rectangle note$10;
    @FXML
    public ComboBox<String> extractor_comboBox;
    @FXML
    public Slider size_states;
    @FXML
    public CheckBox simpleGraph_CheckBox;
    @FXML
    public CheckBox transformered_CheckBox;
    @FXML
    public ComboBox<String> harmonia_comboBox;
    @FXML
    public ComboBox<String> interpretationStrategy_comboBox;
    @FXML
    private ComboBox<SoundSource> soundSource_comboBox;

    private FileChooser fileChooser;


    @Autowired
    private PlayerService playerService;
    @Autowired
    private CaService caService;
    @Autowired
    private InstrumentService instrumentService;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private SoundManagerService soundManagerService;
    @Autowired
    private ConfigService configService;
    @Autowired
    private InterpreterService interpreterService;

    @FXML
    public void initialize() throws Exception {
        //--------------------------------------buttons
        playerService.stoppedProperty().addListener((observable, oldValue, newValue) -> Platform.runLater(() -> {
            stop_button.setDisable(newValue);
            start_button.setDisable(!newValue);
        }));
        start_button.setDisable(configService.getLastFile() == null);
        //--------------------------------------Ca
        caService.setDrawArea(drawArea);
        selectCa(configService.getCaName());
        setDefaultLivingReviveFromTo();
        ChangeListener<Boolean> changeListener = (observable, oldValue, newValue) -> {
            if (!newValue)
                selectLivingDiapasons();
        };
        livingValue1_test.focusedProperty().addListener(changeListener);
        livingValue2_test.focusedProperty().addListener(changeListener);
        reviveValue1_test.focusedProperty().addListener(changeListener);
        reviveValue2_test.focusedProperty().addListener(changeListener);
        //--------------------------------------FileChooser
        fileChooser = new FileChooser();
        if (configService.getLastFile() != null && configService.getLastFile().exists())
            selectFile(configService.getLastFile());
        else
            selectFile(null);
        for (FileChooser.ExtensionFilter extensionFilter : playerService.getExtensionFilter()) {
            fileChooser.getExtensionFilters().add(extensionFilter);
        }
        fileChooser.setTitle(BaseConstants.TITLE_FILE_CHOOSER);
        //--------------------------------------sliders
        size_states.valueProperty().addListener(new ChangeListenerForSliders(val -> selectCellSize(val.doubleValue())));
        size_states.setValue(configService.getCellSize());
        size_slider0.valueProperty().addListener(new ChangeListenerForSliders(val -> selectSize(0, val.intValue())));
        size_slider0.setValue(configService.getSizes()[0]);
        size_slider1.valueProperty().addListener(new ChangeListenerForSliders(val -> selectSize(1, val.intValue())));
        size_slider1.setValue(configService.getSizes()[1]);
        size_slider2.valueProperty().addListener(new ChangeListenerForSliders(val -> selectSize(2, val.intValue())));
        size_slider2.setValue(configService.getSizes()[2]);
        max_states.valueProperty().addListener(new ChangeListenerForSliders(val -> selectMaxStates(val.intValue())));
        max_states.setValue(configService.getMaxState());
        caService.setState();
        //---------------------------------------checkBoxes
        simpleGraph_CheckBox.setSelected(configService.isSimpleGraphics());
        selectIsGraphicsSimple(configService.isSimpleGraphics());
        transformered_CheckBox.setSelected(configService.isTransformered());
        selectIsTransformered(configService.isTransformered());
        //--------------------------------------progress
        playerService.setProgressProperty(progress.progressProperty());
        //--------------------------------------comboBox
        extractor_comboBox.getItems().addAll(applicationContext.getBeanNamesForType(Extractor.class));
        extractor_comboBox.setValue(configService.getExtractor());
        selectExtractor(configService.getExtractor());
        harmonia_comboBox.getItems().addAll(applicationContext.getBeanNamesForType(Harmonia.class));
        harmonia_comboBox.setValue(configService.getHarmonia());
        selectHarmonia(configService.getHarmonia());
        //--------------------------------------notes
        instrumentService.setNodes(note$0, note$1, note$2, note$3, note$4, note$5, note$6, note$7, note$8, note$9, note$10, note$11);
        //-------------------------------------instrument
        selectInstrument(configService.getInstrument());
        selectOctave(configService.getOctave());
        selectTransformerStrategy(configService.getTranslatorStrategy());
        //--------------------------------------sound
        soundSource_comboBox.getItems().addAll(SoundSource.values());
        soundSource_comboBox.setValue(configService.getSoundSource());
        selectSoundSource(configService.getSoundSource());
        //--------------------------------------interpretation
        interpretationStrategy_comboBox.getItems().addAll(applicationContext.getBeanNamesForType(CaInterpreterStrategy.class));
        interpretationStrategy_comboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            interpreterService.setStrategy(newValue);
            configService.setInterpreterStrategy(newValue);
        });
        interpretationStrategy_comboBox.setValue(configService.getInterpreterStrategy());
        //--------------------------------------area

    }

    private void selectSoundSource(SoundSource soundSource) {
        System.out.println("set SoundSource " + soundSource);
        configService.setSoundSource(soundSource);
        soundManagerService.setSoundSource(soundSource);
    }

    private void selectTransformerStrategy(String translator) {
        System.out.println("set TransformerStrategy " + translator);
        configService.setTranslatorStrategy(translator);
        playerService.setTransformerStrategy(translator);
        instrumentService.setTransformerStrategy(translator);
    }

    private void selectIsTransformered(boolean selected) {
        System.out.println("set Transformered = " + selected);
        configService.setTransformered(selected);
        playerService.setIsTransformer(selected);
        instrumentService.setIsTransformer(selected);
    }

    public void selectLivingDiapasons() {
        try {
            setLivingReviveFromTo(Integer.parseInt(livingValue1_test.getText()), Integer.parseInt(livingValue2_test.getText()),
                    Integer.parseInt(reviveValue1_test.getText()), Integer.parseInt(reviveValue2_test.getText()));
        } catch (NumberFormatException ex) {
            setDefaultLivingReviveFromTo();
        }
    }

    private void selectCellSize(double size) {
        System.out.println("set Cell size = " + size);
        configService.setCellSize(size);
        caService.getDrawer().setCellSize(size);
    }

    private void selectIsGraphicsSimple(boolean flag) {
        System.out.println("set Simple Graphics = " + flag);
        configService.setSimpleGraphics(flag);
        caService.getDrawer().setIsGraphicsSimple(flag);
    }

    private void selectMaxStates(int maxState) {
        System.out.println("set maxStates = " + maxState);
        configService.setMaxState(maxState);
        caService.setMaxState(maxState);
    }

    private void selectSize(int index, int size) {
        System.out.println("set size[" + index + "] = " + size);
        caService.setSize(index, size);
        configService.getSizes()[index] = size;
    }

    private void selectOctave(byte octave) {
        instrumentService.setOctave(octave);
        configService.setOctave(octave);
        System.out.println("set Octave " + octave);
    }

    private void selectExtractor(String extractor) {
        System.out.println("set Extractor " + extractor);
        configService.setExtractor(extractor);
        playerService.setExtractor(extractor);
        instrumentService.setExtractor(extractor);
    }

    private void selectHarmonia(String value) {
        System.out.println("set Harmonia " + value);
        configService.setHarmonia(value);
        caService.setHarmonia(value);
    }

    private void selectCa(String caName) {
        System.out.println("set Ca " + caName);
        caService.setCa(caName);
    }

    private void selectInstrument(String instrumentName) {
        System.out.println("set Instrument " + instrumentName);
        instrumentService.setInstrumentVoice(instrumentName);
    }

    private void selectFile(File file) {
        System.out.println("Choose file ---> " + file);
        if (file != null) {
            if (configService.getLastFile() == null)
                start_button.setDisable(false);
            choseFile_field.setText(file.toString());
            configService.setLastFile(file);
            fileChooser.setInitialDirectory(new File(file.getParent()));
            playerService.setFile(file);
        }
    }

    @FXML
    public void onSetExtractorAction(ActionEvent actionEvent) {
        selectExtractor(extractor_comboBox.getValue());
    }

    @FXML
    public void onButtonStartClick(ActionEvent event) {
        playerService.play();
    }

    @FXML
    public void onButtonCancelClick(Event event) {
        playerService.stop();
    }

    @FXML
    public void onButtonChoseFileClick(Event event) {
        selectFile(fileChooser.showOpenDialog(root.getScene().getWindow()));
    }

    @FXML
    public void onRandomStatesOfCells(Event actionEvent) {
        caService.setRandomBeginStates();
    }

    @FXML
    public void onClear(Event actionEvent) {
        caService.clear();
    }

    @FXML
    public void onTestButtonAction(Event actionEvent) {
        caService.setTestState();
    }

    @FXML
    public void onNoteMousePressed(MouseEvent event) {
        instrumentService.noteByNodePressed((Node) event.getTarget());
    }

    @FXML
    public void onNoteMouseReleased(MouseEvent event) {
        instrumentService.noteByNodeReleased((Node) event.getTarget());
    }

    @FXML
    public void onActionInLivingDiapasonsFields(ActionEvent actionEvent) {
        root.requestFocus();
    }

    @FXML
    public void onNoteKeyPressed(KeyEvent event) {
        instrumentService.noteByKeyPressed(event.getCode());
    }

    @FXML
    public void onNoteKeyReleased(KeyEvent event) {
        processControlButtons(event);
        instrumentService.noteByKeyReleased(event.getCode());
    }

    @FXML
    public void onActionSimpleGraph(ActionEvent actionEvent) {
        selectIsGraphicsSimple(simpleGraph_CheckBox.isSelected());
    }


    @FXML
    public void onActionIsTransformered(ActionEvent actionEvent) {
        selectIsTransformered(transformered_CheckBox.isSelected());
    }

    @FXML
    public void onActionHarmonia(ActionEvent actionEvent) {
        selectHarmonia(harmonia_comboBox.getValue());
    }

    @FXML
    public void onActionSoundSource(ActionEvent actionEvent) {
        selectSoundSource(soundSource_comboBox.getValue());
    }

    public void setDefaultLivingReviveFromTo() {
        System.out.println("setLivingValues = " + configService.getLivingFrom() + " - " + configService.getLivingTo());
        System.out.println("setReviveValues = " + configService.getReviveFrom() + " - " + configService.getReviveTo());
        caService.setLivingDiapason(configService.getLivingFrom(), configService.getLivingTo());
        caService.setReviveDiapason(configService.getReviveFrom(), configService.getReviveTo());
        livingValue1_test.setText(Integer.toString(configService.getLivingFrom()));
        livingValue2_test.setText(Integer.toString(configService.getLivingTo()));
        reviveValue1_test.setText(Integer.toString(configService.getReviveFrom()));
        reviveValue2_test.setText(Integer.toString(configService.getReviveTo()));
    }

    public void setLivingReviveFromTo(int lFrom, int lTo, int rFrom, int rTo) {
        if (lFrom > lTo) {
            int temp = lFrom;
            lFrom = lTo;
            lTo = temp;
        }
        if (rFrom > rTo) {
            int temp = rFrom;
            rFrom = rTo;
            rTo = temp;
        }
        configService.setLivingFrom(lFrom);
        configService.setLivingTo(lTo);
        configService.setReviveFrom(rFrom);
        configService.setReviveTo(rTo);
        setDefaultLivingReviveFromTo();
    }

    private void processControlButtons(KeyEvent event) {
        if (event.getCode() == KeyCode.ESCAPE) {
            pressButton(random_button);
        } else if (event.getCode() == KeyCode.SHIFT) {
            pressButton(test_button);
        }
    }

    private void pressButton(Button btn) {
        btn.fire();
        btn.requestFocus();
    }


    private static class ChangeListenerForSliders implements ChangeListener<Number> {
        private Action action;

        public ChangeListenerForSliders(Action action) {
            this.action = action;
        }

        @Override
        public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
            action.duAction(newValue);
        }

        public interface Action {
            void duAction(Number val);
        }
    }
}
