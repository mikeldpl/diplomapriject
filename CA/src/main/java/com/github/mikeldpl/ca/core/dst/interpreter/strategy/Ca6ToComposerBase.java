package com.github.mikeldpl.ca.core.dst.interpreter.strategy;

import com.github.mikeldpl.ca.core.ca.Ca6Area;
import com.github.mikeldpl.ca.core.composer.CommandComposer;
import com.github.mikeldpl.ca.core.composer.Composer;
import com.github.mikeldpl.ca.core.shell.sound.SoundInfo;

public abstract class Ca6ToComposerBase implements Ca6InterpreterToSoundStrategy,
        ComposerContainer {

    private Composer composer;

    @Override
    public SoundInfo interpret(Ca6Area area) {
        return composer.compose(extractComposerCommand(area));
    }

    public Composer getComposer() {
        return composer;
    }

    public void setComposer(Composer composer) {
        this.composer = composer;
    }

    protected abstract CommandComposer extractComposerCommand(Ca6Area area);
}
