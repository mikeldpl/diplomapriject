package com.github.mikeldpl.ca.core.transformer;

import com.github.mikeldpl.ca.core.extractor.ExtractorMidi;

import javax.sound.midi.Sequence;

public interface TransformerMidi extends Transformer<Sequence, Sequence, ExtractorMidi, TransformerStrategyMidi> {
}
