package com.github.mikeldpl.ca.core.extractor;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.sound.midi.MetaMessage;

@Component("Poly Universal Extractor")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ExtractorMidiPolyUniversal extends ExtractorMidiPolyTwoThread implements ExtractorMidi, ExtractorPoly<MetaMessage>, ExtractorPolyOneThread<MetaMessage> {

    private long maxDelayBetweenNotesThatPlayedTogetherInTicks = MAX_DELAY_BETWEEN_NOTES_THAT_PLAYED_TOGETHER_DEFAULT_IN_TICKS;

    public long getMaxDelayBetweenNotesThatPlayedTogetherInTicks() {
        return maxDelayBetweenNotesThatPlayedTogetherInTicks;
    }

    public void setMaxDelayBetweenNotesThatPlayedTogetherInTicks(long maxDelayBetweenNotesThatPlayedTogether) {
        this.maxDelayBetweenNotesThatPlayedTogetherInTicks = maxDelayBetweenNotesThatPlayedTogether;
    }

    @Override
    public void extract(MetaMessage meta) {
        if (meta.getType() == MidiMetaMessageUtil.NOTE_ON_MESSAGE) {
            ((StarterOfCaProcessUniversal) starterOfCaProcess).nowElement(MidiMetaMessageUtil.getNote(meta), MidiMetaMessageUtil.getTicks(meta));
        }
    }

    @Override
    protected ExtractorMidiPolyUniversal.StarterOfCaProcessUniversal buildStarterOfCaProcess() {
        return new StarterOfCaProcessUniversal();
    }

    private class StarterOfCaProcessUniversal extends StarterOfCaProcess {
        private long lastPressedTime;

        public StarterOfCaProcessUniversal() {
            this("StarterOfCaProcess Universal");
        }

        protected StarterOfCaProcessUniversal(String s) {
            super(s);
        }

        public synchronized void nowElement(byte note, long ticks) {
            if (ticks - lastPressedTime >= maxDelayBetweenNotesThatPlayedTogetherInTicks) {
                draw();
            }
            lastPressedTime = ticks;
            nowElement(note);
        }
    }
}
