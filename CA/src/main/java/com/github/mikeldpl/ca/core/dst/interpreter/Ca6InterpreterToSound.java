package com.github.mikeldpl.ca.core.dst.interpreter;

import com.github.mikeldpl.ca.core.sound.SoundSource;
import com.github.mikeldpl.ca.core.ca.Ca6Area;
import com.github.mikeldpl.ca.core.dst.interpreter.strategy.Ca6InterpreterToSoundStrategy;
import com.github.mikeldpl.ca.core.dst.interpreter.strategy.CaInterpreterStrategy;
import com.github.mikeldpl.ca.core.shell.sound.SoundInfo;
import com.github.mikeldpl.ca.core.shell.sound.SoundShell;
import com.github.mikeldpl.ca.core.sound.SoundAwareBase;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Ca6InterpreterToSound
        extends CaInterpreterBase<Ca6Area, SoundInfo, SoundShell, Ca6InterpreterToSoundStrategy>
        implements Ca6Interpreter<SoundInfo, SoundShell, Ca6InterpreterToSoundStrategy>,
        SoundAwareBase{

    @Override
    public Class<? extends CaInterpreterStrategy> getStrategyClass() {
        return Ca6InterpreterToSoundStrategy.class;
    }

    @Override
    public Class<? extends SoundShell> getOutputListenerClass() {
        return SoundShell.class;
    }

    @Override
    public void close() {
        for (SoundShell soundShell : getObservers()) {
            soundShell.close();
        }
    }

    @Override
    public void mute(boolean isMuted) {
        for (SoundShell soundShell : getObservers()) {
            soundShell.mute(isMuted);
        }
    }

    @Override
    public SoundSource getSoundSource() {
        return SoundSource.DESTINATION;
    }
}
