package com.github.mikeldpl.ca.exception;

public class HarmonyCaException extends CaException {
    public HarmonyCaException() {
    }

    public HarmonyCaException(String message) {
        super(message);
    }

    public HarmonyCaException(String message, Throwable cause) {
        super(message, cause);
    }

    public HarmonyCaException(Throwable cause) {
        super(cause);
    }
}
