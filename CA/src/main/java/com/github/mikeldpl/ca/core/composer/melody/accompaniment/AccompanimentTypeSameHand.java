package com.github.mikeldpl.ca.core.composer.melody.accompaniment;

import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Same Hand")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AccompanimentTypeSameHand implements AccompanimentType {
    @Override
    public Chord shiftChord(Chord baseChord) {
        return baseChord;
    }
}
