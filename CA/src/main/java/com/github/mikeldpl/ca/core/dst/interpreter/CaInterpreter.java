package com.github.mikeldpl.ca.core.dst.interpreter;

import com.github.mikeldpl.ca.core.dst.CaDestination;
import com.github.mikeldpl.ca.core.dst.interpreter.strategy.CaInterpreterStrategy;
import com.github.mikeldpl.ca.core.Observable;
import com.github.mikeldpl.ca.core.Observer;
import com.github.mikeldpl.ca.core.ca.CaArea;

public interface CaInterpreter<I extends CaArea<?>, O, L extends Observer<O>, S extends CaInterpreterStrategy<I, O>>
        extends CaDestination<I>, Observable<O, L> {

    void setStrategy(S strategy);

    Class<? extends CaInterpreterStrategy> getStrategyClass();

    Class<? extends Observer> getOutputListenerClass();

    void close();
}
