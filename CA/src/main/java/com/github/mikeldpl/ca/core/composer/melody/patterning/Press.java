package com.github.mikeldpl.ca.core.composer.melody.patterning;

import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;

public class Press {
    ChordMask chordMask;
    int duration;

    public Press(ChordMask chordMask, int duration) {
        this.chordMask = chordMask;
        this.duration = duration;
    }
    public Press(int duration) {
        this(Chord::getStapes, duration);
    }
}
