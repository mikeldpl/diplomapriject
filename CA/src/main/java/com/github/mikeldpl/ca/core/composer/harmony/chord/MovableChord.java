package com.github.mikeldpl.ca.core.composer.harmony.chord;

import com.github.mikeldpl.ca.consts.MusicConstants;
import com.github.mikeldpl.ca.core.composer.Note;
import com.github.mikeldpl.ca.exception.MusicCaException;

public class MovableChord implements Chord {

    private int[] pos;
    private String baseName;

    public static MovableChord fromBaseChordAndStage(Chord chord, Note stage) {
        MovableChord movableChord = new MovableChord(chord);
        for (int i = 0; i < movableChord.pos.length; i++) {
            movableChord.pos[i] += stage.ordinal();
        }
        return movableChord;
    }

    public static MovableChord octaveDown(Chord chord, int octaves) {
        if (octaves <= 0) {
            throw new MusicCaException();
        }
        MovableChord movableChord = new MovableChord(chord);
        for (int i = 0; i < movableChord.pos.length; i++) {
            movableChord.pos[i] -= MusicConstants.NOTES_LENGTH * octaves;
            if (movableChord.pos[i] < 0) {
                throw new MusicCaException();
            }
        }
        return movableChord;
    }

    public MovableChord(Chord chord) {
        baseName = chord.getName();
        pos = chord.getStapes().clone();
    }

    @Override
    public String getId() {
        return getName();
    }

    @Override
    public String getName() {
        return pos[0] + baseName;
    }

    @Override
    public int[] getStapes() {
        return pos;
    }

    public void moveLess(int position) {
        while (!(pos[0] == position || pos[0] < position && pos[1] > position)) {
            if (!(pos[0] > position ? shiftLeft() : shiftRight())) {
                break;
            }
        }
    }

    public void moveHigher(int position) {
        while (!(pos[0] == position || pos[0] > position && pos[pos.length - 1] - MusicConstants.NOTES_LENGTH < position)) {
            if (!(pos[0] < position ? shiftRight() : shiftLeft())) {
                break;
            }
        }
    }

    private boolean shiftRight() {
        int first = pos[0] + MusicConstants.NOTES_LENGTH;
        if (first < MusicConstants.ALL_NOTES_LENGTH) {
            int i = 0;
            for (; i < pos.length - 1; i++) {
                pos[i] = pos[i + 1];
            }
            pos[i] = first;
            return true;
        }
        return false;
    }

    private boolean shiftLeft() {
        int last = pos[pos.length - 1] - MusicConstants.NOTES_LENGTH;
        if (last > 0) {
            int i = pos.length - 1;
            for (; i > 0; i--) {
                pos[i] = pos[i - 1];
            }
            pos[0] = last;
            return true;
        }
        return false;
    }
}
