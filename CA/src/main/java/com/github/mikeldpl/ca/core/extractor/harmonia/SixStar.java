package com.github.mikeldpl.ca.core.extractor.harmonia;

import org.springframework.stereotype.Component;

@Component("SixStar")
public class SixStar implements Harmonia {
    private static int[] val = new int[]{0, 1, 2, 3, 4, 5, 1, 0, 3, 2, 5, 4};

    //                                0  1  2  3  4  5  6  7  8  9 10 11
    @Override
    public int[] get() {
        return val;
    }
}
