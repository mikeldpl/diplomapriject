package com.github.mikeldpl.ca.core.dst.interpreter.strategy;

import com.github.mikeldpl.ca.core.ca.Ca6Area;
import com.github.mikeldpl.ca.core.shell.sound.SoundInfo;

public interface Ca6InterpreterToSoundStrategy extends CaInterpreterStrategy<Ca6Area, SoundInfo> {

}
