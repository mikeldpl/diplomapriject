package com.github.mikeldpl.ca.core.extractor.harmonia;

import org.springframework.stereotype.Component;

@Component("StrangeMAIN")
public class Strange implements Harmonia {
    private static int[] val = new int[]{0, 1, 2, 3, 4, 5, 0, 1, 2, 3, 4, 5};

    @Override
    public int[] get() {
        return val;
    }
}
