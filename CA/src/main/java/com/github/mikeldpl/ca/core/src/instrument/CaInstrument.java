package com.github.mikeldpl.ca.core.src.instrument;

import com.github.mikeldpl.ca.core.extractor.Extractor;
import com.github.mikeldpl.ca.core.sound.SoundAwareBase;
import com.github.mikeldpl.ca.core.sound.SoundSource;
import com.github.mikeldpl.ca.core.transformer.TransformerStrategy;
import com.github.mikeldpl.ca.core.src.CaSource;


public interface CaInstrument<EXT extends Extractor, STRAT extends TransformerStrategy> extends CaSource<EXT>, SoundAwareBase {

    void keyDown(byte key);

    void keyUp(byte key);

    void setInstrumentVoice(String instrument);

    String[] getInstrumentVoices();

    STRAT getTransformerStrategy();

    void setTransformerStrategy(STRAT transformerStrategy);

    Class<STRAT> getStrategyType();

    void setIsTransformer(boolean isTransformer);

    @Override
    default SoundSource getSoundSource() {
        return SoundSource.SOURCE;
    }
}
