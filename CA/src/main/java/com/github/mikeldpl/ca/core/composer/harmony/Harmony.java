package com.github.mikeldpl.ca.core.composer.harmony;

import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;
import com.github.mikeldpl.ca.core.composer.harmony.chord.FunctionalChord;

import java.util.Collection;

public interface Harmony {

    int STAGES_COUNT = 7;
    int HALF_TON = 1;
    int TON = 2;

    String getName();
    Collection<FunctionalChord> getCords();
    int getPositionByStage(int stage);
    PossibleChords getPossibleHarmoniousChords(Chord chord);
}
