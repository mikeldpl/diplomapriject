package com.github.mikeldpl.ca;

import com.github.mikeldpl.ca.consts.BaseConstants;
import com.github.mikeldpl.ca.controller.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;
import java.io.IOException;
import java.io.InputStream;

public class StartPoint extends Application {

    public static void main(String[] args) throws InvalidMidiDataException, IOException, MidiUnavailableException {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(ContextConfig.class);

        stage.setTitle(BaseConstants.TITLE);
        stage.setScene(initRootFromFXML(stage, context));
        stage.getIcons().add(new Image(String.valueOf(getClass().getResource("/img/icon.png"))));
        stage.show();
    }

    private Scene initRootFromFXML(Stage stage, ConfigurableApplicationContext context) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();

        fxmlLoader.setControllerFactory(context::getBean);
        fxmlLoader.setLocation(getClass().getResource(BaseConstants.MAIN_FXML));
        GridPane grid;
        try (InputStream is = getClass().getResourceAsStream(BaseConstants.MAIN_FXML)) {
            grid = fxmlLoader.load(is);
        }
        MainController controller = fxmlLoader.getController();
        stage.setOnCloseRequest(event -> context.close());
        stage.setMaximized(true);
        return new Scene(grid);
    }
}