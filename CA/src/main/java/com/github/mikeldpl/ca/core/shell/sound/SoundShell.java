package com.github.mikeldpl.ca.core.shell.sound;

import com.github.mikeldpl.ca.core.shell.Shell;
import com.github.mikeldpl.ca.core.sound.Mutable;

import java.util.function.IntConsumer;

public interface SoundShell extends Shell<SoundInfo>, Mutable {

    void setInstrumentVoice(String instrument);

    String[] getInstrumentVoices();

    void fire(SoundInfo soundInfo, IntConsumer preProcess);
}
