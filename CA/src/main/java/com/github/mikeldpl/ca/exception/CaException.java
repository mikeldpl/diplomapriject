package com.github.mikeldpl.ca.exception;


public class CaException extends RuntimeException {
    public CaException() {
    }

    public CaException(String message) {
        super(message);
    }

    public CaException(String message, Throwable cause) {
        super(message, cause);
    }

    public CaException(Throwable cause) {
        super(cause);
    }
}
