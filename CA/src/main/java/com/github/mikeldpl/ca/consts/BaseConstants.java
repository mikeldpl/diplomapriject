package com.github.mikeldpl.ca.consts;

import com.github.mikeldpl.ca.service.ConfigService;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Paths;

public class BaseConstants {

    public static final File CONFIG_FILE = getPath();
    public static final String MAIN_FXML = "/views/main.fxml";
    public static final String TITLE = "CA";
    public static final String TITLE_FILE_CHOOSER = "Open Resource File";

    private BaseConstants() {
    }

    private static File getPath() {
        try {
            return Paths.get(ConfigService.class.getProtectionDomain().getCodeSource().getLocation().toURI())
                    .resolveSibling("config.xml").toFile();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }
}
