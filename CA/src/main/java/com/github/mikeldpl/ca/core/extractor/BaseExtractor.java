package com.github.mikeldpl.ca.core.extractor;

import com.github.mikeldpl.ca.consts.MusicConstants;
import com.github.mikeldpl.ca.core.ca.Ca;

import java.util.Arrays;

public abstract class BaseExtractor<IN> implements Extractor<IN> {
    private Ca ca;
    private volatile boolean haveDraw;
    private int[] message = new int[MusicConstants.NOTES_LENGTH];


    protected int[] getMessage() {
        return message;
    }

    protected void addNoteToMessage(byte note) {
        haveDraw = true;
        message[note % MusicConstants.NOTES_LENGTH]++;
    }

    protected void draw() {
        if (haveDraw) {
            getCa().inMessage(message.clone());
            Arrays.fill(message, 0);
            haveDraw = false;
        }
    }

    @Override
    public Ca getCa() {
        return ca;
    }

    @Override
    public void setCa(Ca ca) {
        this.ca = ca;
    }

    @Override
    public void close() {
    }
}
