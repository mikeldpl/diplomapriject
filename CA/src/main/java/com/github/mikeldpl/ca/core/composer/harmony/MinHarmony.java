package com.github.mikeldpl.ca.core.composer.harmony;

import com.github.mikeldpl.ca.core.composer.harmony.chord.FunctionalChordDefault;
import com.github.mikeldpl.ca.core.composer.harmony.chord.ChordType;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Minor")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MinHarmony extends HarmonyBase {

    private final FunctionalChordDefault t = new FunctionalChordDefault("t", ChordType.M53, getPositionByStage(0), this);
    private final FunctionalChordDefault II = new FunctionalChordDefault("II", ChordType.Dimin53, getPositionByStage(1), this);
    private final FunctionalChordDefault IIdd = new FunctionalChordDefault("II##", ChordType.B53, getPositionByStage(1), this);
    private final FunctionalChordDefault IId = new FunctionalChordDefault("II#", ChordType.M53, getPositionByStage(1), this);
    private final FunctionalChordDefault III = new FunctionalChordDefault("III", ChordType.B53, getPositionByStage(2), this);
    private final FunctionalChordDefault IIIb = new FunctionalChordDefault("IIIb", ChordType.M53, getPositionByStage(2), this);
    private final FunctionalChordDefault s = new FunctionalChordDefault("s", ChordType.M53, getPositionByStage(3), this);
    private final FunctionalChordDefault sd = new FunctionalChordDefault("s#", ChordType.B53, getPositionByStage(3), this);
    private final FunctionalChordDefault dd = new FunctionalChordDefault("d#", ChordType.B53, getPositionByStage(4), this);
    private final FunctionalChordDefault d = new FunctionalChordDefault("d", ChordType.M53, getPositionByStage(4), this);
    private final FunctionalChordDefault VI = new FunctionalChordDefault("VI", ChordType.B53, getPositionByStage(5), this);
    private final FunctionalChordDefault VId = new FunctionalChordDefault("VI#", ChordType.M53, getPositionByStage(5) + 1, this);
    private final FunctionalChordDefault VIb = new FunctionalChordDefault("VIb", ChordType.M53, getPositionByStage(5), this);
    private final FunctionalChordDefault VII = new FunctionalChordDefault("VII", ChordType.B53, getPositionByStage(6), this);
    private final FunctionalChordDefault VIIb = new FunctionalChordDefault("VIIb", ChordType.M53, getPositionByStage(6), this);
    private final FunctionalChordDefault D7 = new FunctionalChordDefault("D7", ChordType.D7, getPositionByStage(4), this);
    private final FunctionalChordDefault M57d = new FunctionalChordDefault("M57#", ChordType.CmM7, getPositionByStage(1), this);
    private final FunctionalChordDefault M57 = new FunctionalChordDefault("M57", ChordType.Cmin7, getPositionByStage(1), this);
    private final FunctionalChordDefault M57dd = new FunctionalChordDefault("M57##", ChordType.Cdim7, getPositionByStage(6) + 1, this);

    public MinHarmony() {
        super(0, TON, HALF_TON, TON, TON, HALF_TON, TON, TON);
        this
                .addCords(t, II, s, d, dd, III, VI, VII, D7, M57d, M57, M57dd, IId)
                .addCords(II, t, III, s, dd, VId)
                .addCords(IIdd, d, dd, VId)
                .addCords(IId, t, d, dd, VII, sd)
                .addCords(III, t, II, s, d, VI, VII, VIIb, M57d, M57, dd)
                .addCords(IIIb, sd, VII, VIIb)
                .addCords(s, t, d, dd, VI, VII, VIIb, III, II, VIb)
                .addCords(sd, IId, VId, VII, VIIb)
                .addCords(dd, II, III, t, M57d, M57, M57dd, VId)
                .addCords(d, III, t, s, M57d, M57, M57dd)
                .addCords(VI, s, III, VII, t, VIIb)
                .addCords(VId, II, dd)
                .addCords(VIb, s)
                .addCords(VII, t, III, s, sd, VI, D7)
                .addCords(VIIb, III, s, sd, VI)

                .addCords(D7, t, VII)
                .addCords(M57d, d, dd, t, III)
                .addCords(M57, d, dd, t, III)
                .addCords(M57dd, d, dd, t);
    }

    @Override
    public String getName() {
        return "Minor";
    }
}
