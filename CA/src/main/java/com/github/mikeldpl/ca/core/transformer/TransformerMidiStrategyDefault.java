package com.github.mikeldpl.ca.core.transformer;

import com.github.mikeldpl.ca.consts.MusicConstants;
import com.github.mikeldpl.ca.core.extractor.ExtractorMidi;
import com.github.mikeldpl.ca.core.ca.Ca;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Default midi transformer strategy")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TransformerMidiStrategyDefault implements TransformerStrategyMidi {
    public TransformerMidiStrategyDefault() {
    }

    public byte invert(byte note, int command, long tick, Ca ca, ExtractorMidi extractor) {
        int[] cellRights = ca.getHarmonia().get();
        byte partNote = (byte) (note % MusicConstants.NOTES_LENGTH);
        int right = cellRights[partNote];
        for (byte i = 0; i < cellRights.length; i++) {
            if (right == cellRights[i] && partNote != i)
                return (byte) (i + (byte) (note / MusicConstants.NOTES_LENGTH * MusicConstants.NOTES_LENGTH));
        }
        return note;
    }
}
