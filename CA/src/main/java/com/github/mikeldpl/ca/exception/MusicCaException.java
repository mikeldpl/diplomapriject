package com.github.mikeldpl.ca.exception;

public class MusicCaException extends CaException {
    public MusicCaException() {
    }

    public MusicCaException(String message) {
        super(message);
    }

    public MusicCaException(String message, Throwable cause) {
        super(message, cause);
    }

    public MusicCaException(Throwable cause) {
        super(cause);
    }
}
