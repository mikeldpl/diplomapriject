package com.github.mikeldpl.ca.service;

import com.github.mikeldpl.ca.core.composer.Note;
import com.github.mikeldpl.ca.core.composer.melody.accompaniment.Accompaniment;
import com.github.mikeldpl.ca.core.composer.Composer;
import com.github.mikeldpl.ca.core.composer.harmony.Harmony;
import com.github.mikeldpl.ca.core.composer.melody.MelodyExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
public class ComposerService implements ContainerService {

    @Autowired
    private InterpreterService interpreterService;
    @Autowired
    private ApplicationContext context;
    private Composer composer;
    private double duration;
    private MelodyExtractor melodyExtractor;
    private Note stage;
    private int tact;
    private Harmony harmony;
    private Accompaniment accompaniment;

    @Override
    public Stream getContent() {
        return Stream.of(composer);
    }

    public void setComposer(String name) {
        this.composer = context.getBean(name, Composer.class);
        interpreterService.setComposer(composer);
        initComposer();
    }

    public void setDuration(double duration) {
        this.duration = duration;
        initComposer();
    }

    public void setMelodyExtractor(String melodyExtractorName) {
        this.melodyExtractor = context.getBean(melodyExtractorName, MelodyExtractor.class);
        initComposer();
    }

    public void setAccompaniment(String accompanimentName) {
        this.accompaniment = context.getBean(accompanimentName, Accompaniment.class);
        initComposer();
    }

    public void setStage(Note stage) {
        this.stage = stage;
        initComposer();
    }

    public void setTact(int tact) {
        this.tact = tact;
        initComposer();
    }

    private void initComposer() {
        composer.setDuration((long) (duration * 1000));
        composer.setAccompaniment(accompaniment);
        composer.setStage(stage);
        composer.setTact(tact);
        composer.setMelodyExtractor(melodyExtractor);
        composer.setHarmony(harmony);
        composer.clean();
    }

    public Composer getComposer() {
        return composer;
    }


    public void setHarmony(String harmony) {
        this.harmony = context.getBean(harmony, Harmony.class);
        initComposer();
    }

    public Harmony getHarmony() {
        return harmony;
    }
}
