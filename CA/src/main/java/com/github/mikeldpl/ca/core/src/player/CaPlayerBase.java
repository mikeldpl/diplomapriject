package com.github.mikeldpl.ca.core.src.player;

import com.github.mikeldpl.ca.core.extractor.Extractor;
import com.github.mikeldpl.ca.core.src.BaseCaSource;
import com.github.mikeldpl.ca.core.transformer.Transformer;
import com.github.mikeldpl.ca.exception.CaException;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;

import javax.swing.*;
import java.io.File;

public abstract class CaPlayerBase<EXT extends Extractor, TRANSFORMER extends Transformer>
        extends BaseCaSource<EXT> implements CaPlayer<EXT, TRANSFORMER> {
    private DoubleProperty progressProperty = new SimpleDoubleProperty(0);
    private BooleanProperty stoppedProperty = new SimpleBooleanProperty(true);
    private Timer timer = new Timer(1000, act -> getProgressProperty().set(getProgress()));
    private TRANSFORMER transformer;
    private boolean isTransformed;

    @Override
    public TRANSFORMER getTransformer() {
        return transformer;
    }

    @Override
    public void setTransformer(TRANSFORMER transformer) {
        if (transformer.getExtractorType() != getExtractorType())
            throw new CaException("Invalid transformer was seted");
        this.transformer = transformer;
    }

    @Override
    public boolean isTransformed() {
        return transformer != null && isTransformed;
    }

    @Override
    public void setIsTransformed(boolean isTransformed) {
        this.isTransformed = isTransformed;
    }

    @Override
    public void play(File path) {
        timer.start();
        stoppedProperty.set(false);
    }

    @Override
    public void stop() {
        timer.stop();
        progressProperty.set(0);
        stoppedProperty.set(true);
    }

    public abstract double getProgress();

    @Override
    public DoubleProperty getProgressProperty() {
        return progressProperty;
    }

    @Override
    public void setProgressProperty(DoubleProperty progressProperty) {
        this.progressProperty = progressProperty;
    }

    @Override
    public BooleanProperty getStoppedProperty() {
        return stoppedProperty;
    }

    @Override
    public void setStoppedProperty(BooleanProperty stoppedProperty) {
        this.stoppedProperty = stoppedProperty;
    }

    @Override
    public void close() {
        super.close();
        if (getTransformer() != null)
            getTransformer().close();
        stop();
    }

}
