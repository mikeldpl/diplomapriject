package com.github.mikeldpl.ca.core.shell.text;

import com.github.mikeldpl.ca.core.shell.Shell;

public interface TextShell extends Shell<String> {
}
