package com.github.mikeldpl.ca.core.composer.melody;

import com.github.mikeldpl.ca.core.composer.melody.patterning.ChordMask;
import com.github.mikeldpl.ca.core.composer.melody.patterning.Press;
import com.github.mikeldpl.ca.core.composer.melody.patterning.UsageChord;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Nirvana - SLTS")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MelodyExtractorPattenNirvanaSLTS extends MelodyExtractorPattenBase {

    private UsageChord[] usageChords = new UsageChord[]{
            new UsageChord(new Press(Melody.ONE_16 * 3), new Press(Melody.ONE_16), new Press(Melody.ONE_4)),
            new UsageChord(new Press(ChordMask.pause(), Melody.ONE_8), new Press(Melody.ONE_8), new Press(Melody.ONE_8), new Press(ChordMask.first(2), Melody.ONE_8)),
            new UsageChord(new Press(Melody.ONE_16 * 3), new Press(Melody.ONE_16), new Press(Melody.ONE_4)),
            new UsageChord(new Press(ChordMask.pause(), Melody.ONE_8), new Press(ChordMask.powerUp(1), Melody.ONE_8), new Press(ChordMask.powerUp(1), Melody.ONE_4))
    };

    @Override
    protected UsageChord getTemplateMelody(double temperament, int index) {
        return usageChords[index % usageChords.length];
    }
}
