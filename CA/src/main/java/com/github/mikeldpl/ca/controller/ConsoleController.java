package com.github.mikeldpl.ca.controller;

import com.github.mikeldpl.ca.core.shell.text.TextShellTextArea;
import com.github.mikeldpl.ca.service.ConfigService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller
public class ConsoleController {

    @Autowired
    @Qualifier("default")
    private TextShellTextArea textShellTextArea;
    @Autowired
    private ConfigService configService;
    @FXML
    public CheckBox wrap;
    @FXML
    private TextArea consoleArea;
    @FXML
    private CheckBox follow;
    @FXML
    private Button cleanButton;

    @FXML
    public void initialize() throws Exception {
        textShellTextArea.setTextArea(consoleArea);

        wrap.setSelected(configService.isConsoleWrapping());
        consoleArea.setWrapText(configService.isConsoleWrapping());

        follow.setSelected(configService.isConsoleFollow());
        textShellTextArea.setFollow(configService.isConsoleFollow());
    }

    public void followAction(ActionEvent actionEvent) {
        boolean newValue = follow.isSelected();
        System.out.println("set Follow = " + newValue);
        textShellTextArea.setFollow(newValue);
        configService.setConsoleFollow(newValue);
    }

    public void wrapAction(ActionEvent actionEvent) {
        boolean newValue = wrap.isSelected();
        System.out.println("set Wrap = " + newValue);
        consoleArea.setWrapText(newValue);
        configService.setConsoleWrapping(newValue);
    }

    public void cleanButtonAction(ActionEvent actionEvent) {
        consoleArea.clear();
    }
}
