package com.github.mikeldpl.ca.core;


public interface Observer<A> {
    void notifyByObservable(A info);

}
