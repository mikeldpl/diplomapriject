package com.github.mikeldpl.ca.core.composer;

public enum Note {
    C("C"), Cd("C#"), D("D"), Dd("D#"), E("E"), F("F"),
    Fd("F#"), G("G"), Gd("G#"), A("A"), Ad("A#"), B("B");

    private String id;

    Note(String id) {
        this.id = id;
    }

    String getId() {
        return id;
    }
}