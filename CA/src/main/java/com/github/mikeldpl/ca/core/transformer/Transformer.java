package com.github.mikeldpl.ca.core.transformer;

import com.github.mikeldpl.ca.core.extractor.Extractor;
import com.github.mikeldpl.ca.core.src.CaSource;

public interface Transformer<OUT, IN, EXT extends Extractor, STRATEGY extends TransformerStrategy> extends CaSource<EXT> {
    void transform(IN in, OUT out);

    STRATEGY getTransformerStrategy();

    void setTransformerStrategy(STRATEGY transformerStratege);

    Class<STRATEGY> getStrategyType();
}
