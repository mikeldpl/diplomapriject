package com.github.mikeldpl.ca.core.shell.sound;

import com.github.mikeldpl.ca.exception.CaException;

public class NoteAction {
    byte key;
    long delay;
    boolean isOn;

    public NoteAction(byte key, long delay, boolean isOn) {
        validateByteMoreThen(key);
        this.key = key;
        this.delay = delay;
        this.isOn = isOn;
    }

    public static NoteAction instNoteOn(byte ton, long delay) {
        return new NoteAction(ton, delay, true);
    }

    public static NoteAction instNoteOn(byte ton) {
        return new NoteAction(ton, 0, true);
    }

    public static NoteAction instNoteOff(byte ton, long delay) {
        return new NoteAction(ton, delay, false);
    }

    public static NoteAction instNoteOff(byte ton) {
        return new NoteAction(ton, 0, false);
    }

    private void validateByteMoreThen(byte param) {
        if (param < 0) {
            throw new CaException("Invalid parameter, must be >= 0");
        }
    }
}