package com.github.mikeldpl.ca.core.sound;

public enum SoundSource {
    SOURCE, DESTINATION, ALL {
        @Override
        public boolean shouldMute(SoundSource soundSource) {
            return false;
        }
    };

    public boolean shouldMute(SoundSource soundSource){
        return soundSource != ALL && soundSource != this;
    }
}
