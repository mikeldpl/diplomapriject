package com.github.mikeldpl.ca.service;

import com.github.mikeldpl.ca.consts.BaseConstants;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import javax.xml.bind.JAXB;

@Component
public class ConfigServiceFactory implements FactoryBean<ConfigService> {

    private ConfigService configService;

    @Override
    public ConfigService getObject() throws Exception {
        return configService = BaseConstants.CONFIG_FILE.exists() ?
                JAXB.unmarshal(BaseConstants.CONFIG_FILE, ConfigService.class) :
                new ConfigService();
    }

    @Override
    public Class<?> getObjectType() {
        return ConfigService.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @PreDestroy
    public void close() {
        JAXB.marshal(configService, BaseConstants.CONFIG_FILE);
        System.out.println("Options saved");
    }
}
