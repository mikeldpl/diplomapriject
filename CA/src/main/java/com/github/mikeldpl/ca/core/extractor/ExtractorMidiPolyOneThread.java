package com.github.mikeldpl.ca.core.extractor;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.sound.midi.MetaMessage;

@Component("Poly Extractor For MIDI")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ExtractorMidiPolyOneThread extends BaseExtractorPolyOneThread<MetaMessage> implements ExtractorMidi, ExtractorPolyOneThread<MetaMessage> {
    private long lastPressedTime;

    @Override
    public synchronized void extract(MetaMessage message) {
        if (MidiMetaMessageUtil.getType(message) == MidiMetaMessageUtil.NOTE_ON_MESSAGE) {
            long ticks = MidiMetaMessageUtil.getTicks(message);
            if (ticks - lastPressedTime > getMaxDelayBetweenNotesThatPlayedTogetherInTicks()) {
                draw();
            }
            lastPressedTime = ticks;
            addNoteToMessage(MidiMetaMessageUtil.getNote(message));
        } else if (MidiMetaMessageUtil.getType(message) == MidiMetaMessageUtil.END_OF_TRACK_MESSAGE) {
            draw();
        }
    }
}
