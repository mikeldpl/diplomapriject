package com.github.mikeldpl.ca.service;

import com.github.mikeldpl.ca.core.Observer;
import com.github.mikeldpl.ca.core.ca.Ca;
import com.github.mikeldpl.ca.core.composer.Composer;
import com.github.mikeldpl.ca.core.dst.interpreter.CaInterpreter;
import com.github.mikeldpl.ca.core.dst.interpreter.strategy.CaInterpreterStrategy;
import com.github.mikeldpl.ca.core.dst.interpreter.strategy.ComposerContainer;
import com.github.mikeldpl.ca.exception.CaException;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.Collection;
import java.util.stream.Stream;

@Service
public class InterpreterService implements ContainerService<CaInterpreter> {
    @Autowired
    private ApplicationContext context;
    @Autowired
    private ComposerService composerService;
    private Collection<CaInterpreter> interpreters;
    private CaInterpreterStrategy strategy;

    public void setStrategy(String interpreterStrategyName) {
        System.out.println("set CaInterpreterStrategy " + interpreterStrategyName);
        for (CaInterpreter interpreter : interpreters) {
            try {
                strategy = context.getBean(interpreterStrategyName, (Class<CaInterpreterStrategy>) interpreter.getStrategyClass());
                setComposer(composerService.getComposer());
                interpreter.setStrategy(strategy);
                return;
            } catch (BeansException ex) {
                //it is ok
            }
        }
        throw new CaException("Invalid interpreterStrategyName " + interpreterStrategyName);
    }

    private void addCaListenersToInterpreter(CaInterpreter interpreter) {
        for (Observer o : context.getBeansOfType((Class<Observer>) interpreter.getOutputListenerClass()).values()) {
            interpreter.addObserver(o);
        }
    }

    public void addCaListeners(Ca ca) {
        interpreters = context.getBeansOfType((Class<CaInterpreter>) ca.getDefaultInterpreterClass()).values();
        for (CaInterpreter interpreter : interpreters) {
            ca.addObserver(interpreter);
            addCaListenersToInterpreter(interpreter);
        }
    }

    @PreDestroy
    public void close() {
        for (CaInterpreter interpreter : interpreters) {
            interpreter.close();
        }
    }

    @Override
    public Stream<CaInterpreter> getContent() {
        return interpreters.stream();
    }

    public void setComposer(Composer composer) {
        if (strategy instanceof ComposerContainer) {
            ((ComposerContainer)strategy).setComposer(composer);
        }
    }
}
