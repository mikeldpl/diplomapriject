package com.github.mikeldpl.ca.core.composer.harmony;

import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;
import com.github.mikeldpl.ca.core.composer.harmony.chord.FunctionalChord;
import com.github.mikeldpl.ca.exception.HarmonyCaException;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public abstract class HarmonyBase implements Harmony {

    private int[] stages = new int[STAGES_COUNT];
    private Map<FunctionalChord, Set<FunctionalChord>> functionalChords =
            new LinkedHashMap<>(20);

    public HarmonyBase(int... stagesTones) {
        assert stagesTones.length == STAGES_COUNT;
        int note = 0;
        for (int i = 0; i < STAGES_COUNT; i++) {
            stages[i] = note += stagesTones[i];
        }
    }

    @Override
    public Collection<FunctionalChord> getCords() {
        return functionalChords.keySet();
    }

    @Override
    public int getPositionByStage(int stage) {
        return stages[stage];
    }

    protected HarmonyBase addCords(FunctionalChord functionalChord, FunctionalChord... connections) {
        Set<FunctionalChord> realCons = new HashSet<>();
        for (FunctionalChord connection : connections) {
            realCons.add(connection);
            Set<FunctionalChord> functionalChords = this.functionalChords.get(connection);
            if (functionalChords != null && !functionalChords.contains(functionalChord)) {
                throw new HarmonyCaException("Haven't cyclic connection " + connection + " to " + functionalChord);
            }
        }
        functionalChords.put(functionalChord, realCons);
        return this;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public PossibleChords getPossibleHarmoniousChords(Chord chord) {
        if (chord == null) {
            return new PossibleChordsBasedOnlyOnFunctions(functionalChords.keySet());
        }
        return new PossibleChordsBasedOnlyOnFunctions(functionalChords.get(chord));
    }
}