package com.github.mikeldpl.ca.core.composer.melody.accompaniment;

import com.github.mikeldpl.ca.core.composer.melody.Melody;
import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Reliable Chord")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AccompanimentReliableChord extends AccompanimentBase {

    private static final int DURATION_OF_KEY = Melody.ONE_8;

    @Override
    protected void extractChord(Melody melody, Chord chord, double temperament) {
        int dolesCount = melody.getCommonDuration() / DURATION_OF_KEY;
        if (chord.getStapes().length >= 4) {
            for (int i = 0; i < dolesCount; i++) {
                for (int note : chord.getStapes()) {
                    melody.addInThisMoment(note, DURATION_OF_KEY);
                }
                melody.shiftMoment(DURATION_OF_KEY);
            }
        } else {
            melody.addArpeggio(chord.getStapes(), dolesCount, DURATION_OF_KEY);
        }
    }
}
