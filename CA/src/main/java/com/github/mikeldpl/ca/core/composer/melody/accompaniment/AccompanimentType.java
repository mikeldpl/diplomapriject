package com.github.mikeldpl.ca.core.composer.melody.accompaniment;

import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;

public interface AccompanimentType {
    Chord shiftChord (Chord baseChord);
}
