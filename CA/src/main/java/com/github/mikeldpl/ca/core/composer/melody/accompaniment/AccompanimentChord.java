package com.github.mikeldpl.ca.core.composer.melody.accompaniment;

import com.github.mikeldpl.ca.core.composer.melody.Melody;
import com.github.mikeldpl.ca.core.composer.harmony.chord.Chord;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Chord")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AccompanimentChord extends AccompanimentBase {
    @Override
    protected void extractChord(Melody melody, Chord chord, double temperament) {
        for (int note : chord.getStapes()) {
            melody.addInThisMoment(note, melody.getCommonDuration());
        }
    }

}
