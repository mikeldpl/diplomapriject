package com.github.mikeldpl.ca.consts;

import javafx.scene.paint.Color;

public class WriterConstants {
    public static final Color[] STATE_COLORS = {Color.color(0, 0, 0, 0.15), Color.DIMGREY, Color.DARKSLATEGRAY,
            Color.CYAN, Color.DARKTURQUOISE,
            Color.GOLDENROD, Color.GOLD,
            Color.GREENYELLOW, Color.CHARTREUSE,
            Color.RED, Color.BROWN};
    public static final boolean USE_GRADIENT = true;
    public static final double GRADIENT_OFFSET = 0.5;

    private WriterConstants() {
    }
}
