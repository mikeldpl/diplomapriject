package com.github.mikeldpl.ca.core.ca;

import com.github.mikeldpl.ca.core.dst.CaDestination;
import com.github.mikeldpl.ca.core.dst.drawer.CaDrawer;
import com.github.mikeldpl.ca.core.extractor.harmonia.Harmonia;
import com.github.mikeldpl.ca.core.Observable;
import com.github.mikeldpl.ca.core.dst.interpreter.CaInterpreter;

public interface Ca<A extends CaArea<?>, O extends CaDestination<A>> extends Observable<A, O> {
    A getArea();

    int getNumberSizes();

    int getMinSize();

    int getMaxState();

    void setMaxState(int states);

    void setSize(int i, int size);

    void inMessage(int[] msg);

    void clear();

    Class<? extends CaDrawer> getDefaultDrawerClass();

    void setLivingDiapason(int from, int to);

    void setLivingValues(int... values);

    void setReviveDiapason(int from, int to);

    void setReviveValues(int... values);

    void componRules(int[] rules);

    void process();

    void setRandomBeginStates();

    void setTestStates();

    Harmonia getHarmonia();

    void setHarmonia(Harmonia harmonia);

    Class<? extends CaInterpreter> getDefaultInterpreterClass();

    void setState(int[] sates);

    int[] getState();
}
