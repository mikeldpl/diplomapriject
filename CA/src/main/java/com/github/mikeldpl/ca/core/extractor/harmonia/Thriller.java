package com.github.mikeldpl.ca.core.extractor.harmonia;

import org.springframework.stereotype.Component;

@Component("Thriller")
public class Thriller implements Harmonia {

    private static int[] val = new int[]{0, 5, 1, 0, 2, 1, 3, 2, 4, 3, 5, 4};

    @Override
    public int[] get() {
        return val;
    }
}
