package com.github.mikeldpl.ca.core.ca;

public interface CaArea<T extends Cell> extends Iterable<T> {
    int getDimension();

    int getNumberOfSizes();

    Cell getCell(int... index);

    void setSize(int index, int value);

    int getMinSize();

    void clear();

    int[] getSizes();

    int numberOfCells();

    void setRandomBeginStates();

    void setTestState();

    void setState(int[] sates);

    int[] getState();
}
