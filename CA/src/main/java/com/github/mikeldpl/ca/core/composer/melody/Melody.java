package com.github.mikeldpl.ca.core.composer.melody;

import java.util.ArrayList;
import java.util.Collection;

import static java.lang.Integer.max;

public class Melody {
    public static final int ONE_64 = 1 << 2;
    public static final int ONE_32 = ONE_64 << 1;
    public static final int ONE_16 = ONE_32 << 1;
    public static final int ONE_8 = ONE_16 << 1;
    public static final int ONE_4 = ONE_8 << 1;
    public static final int ONE_2 = ONE_4 << 1;
    public static final int ONE_1 = ONE_2 << 1;

    private Collection<Note> notes = new ArrayList<>(20);
    private int cursor;
    private int commonDuration;

    public Melody addInThisMoment(int value, int duration) {
        notes.add(new Note(value, duration, cursor));
        return addPauseInThisMoment(duration);
    }

    public Melody addPauseInThisMoment(int duration) {
        commonDuration = max(cursor + duration, commonDuration);
        return this;
    }

    public int getCommonDuration() {
        return commonDuration;
    }

    public Melody add16InThisMoment(int value) {
        return addInThisMoment(value, ONE_16);
    }

    public Melody add8InThisMoment(int value) {
        return addInThisMoment(value, ONE_8);
    }

    public Melody add4InThisMoment(int value) {
        return addInThisMoment(value, ONE_4);
    }

    public Melody add2InThisMoment(int value) {
        return addInThisMoment(value, ONE_2);
    }

    public Melody add1InThisMoment(int value) {
        return addInThisMoment(value, ONE_1);
    }

    public Melody shiftMoment(int time) {
        cursor += time;
        return this;
    }

    public Melody shiftBackMoment(int time) {
        return shiftMoment(-time);
    }

    public Collection<Note> getNotes() {
        return notes;
    }

    public void shiftToBegin() {
        cursor = 0;
    }

    public Melody addArpeggio(int[] stapes, int dolesCount, int durationOfKey) {
        int i = 0;
        while (dolesCount > 0 && i < stapes.length) {
            addInThisMoment(stapes[i++], dolesCount-- * durationOfKey);
            shiftMoment(durationOfKey);
        }
        shiftBackMoment(durationOfKey);
        for (; i < stapes.length; i++) {
            addInThisMoment(stapes[i], durationOfKey);
        }
        return this;
    }

    public static class Note {
        public final int value;
        public final int duration;
        public final int moment;

        public Note(int value, int duration, int moment) {
            this.value = value;
            this.duration = duration;
            this.moment = moment;
        }
    }
}
