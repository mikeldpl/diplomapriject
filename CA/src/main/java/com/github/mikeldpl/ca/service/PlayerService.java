package com.github.mikeldpl.ca.service;

import com.github.mikeldpl.ca.core.extractor.Extractor;
import com.github.mikeldpl.ca.core.src.player.CaPlayer;
import com.github.mikeldpl.ca.core.transformer.Transformer;
import com.github.mikeldpl.ca.core.transformer.TransformerStrategy;
import com.github.mikeldpl.ca.exception.CaException;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.stage.FileChooser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

@Service
public class PlayerService implements ContainerService<CaPlayer> {

    @Autowired
    private Set<CaPlayer> players;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private CaService caService;
    private File file;
    private CaPlayer curPlayer;
    private DoubleProperty progressProperty = new SimpleDoubleProperty(0);
    private BooleanProperty stoppedProperty = new SimpleBooleanProperty(true);
    private String extractor;
    private String transformerStrategy;
    private boolean isTransformer;

    public Set<CaPlayer> getPlayers() {
        return players;
    }

    public BooleanProperty stoppedProperty() {
        return stoppedProperty;
    }

    public Set<? extends FileChooser.ExtensionFilter> getExtensionFilter() {
        Set<FileChooser.ExtensionFilter> res = new HashSet<>();
        for (CaPlayer player : players) {
            res.add(player.getExtensionFilterFilter());
        }
        return res;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public void play() {
        if (file == null)
            throw new CaException("File is not set");
        for (CaPlayer player : players) {
            for (String s : player.getExtensionFilterFilter().getExtensions()) {
                if (file.getName().matches("." + s + "$")) {
                    curPlayer = player;
                    player.setCa(caService.getCa());
                    player.setStoppedProperty(stoppedProperty);
                    player.setProgressProperty(progressProperty);
                    player.setExtractor((Extractor) applicationContext.getBean(extractor, player.getExtractorType()));
                    Transformer bean = (Transformer) applicationContext.getBean(player.getTransformerType());
                    bean.setTransformerStrategy((TransformerStrategy) applicationContext.getBean(transformerStrategy, bean.getStrategyType()));
                    player.setTransformer(bean);
                    player.setIsTransformed(isTransformer);
                    player.play(file);
                    break;
                }
            }
        }
    }

    public void stop() {
        curPlayer.stop();
    }

    @PreDestroy
    public void close() {
        for (CaPlayer player : players) {
            player.close();
        }
    }

    public void setTransformerStrategy(String name) {
        transformerStrategy = name;
    }

    public void setIsTransformer(boolean val) {
        isTransformer = val;
    }

    public DoubleProperty getProgressProperty() {
        return progressProperty;
    }

    public void setProgressProperty(DoubleProperty progressProperty) {
        this.progressProperty = progressProperty;
    }

    public String getExtractor() {
        return extractor;
    }

    public void setExtractor(String extractor) {
        this.extractor = extractor;
    }

    @Override
    public Stream<CaPlayer> getContent() {
        return players.stream();
    }
}
