package com.github.mikeldpl.ca.core.composer;

import com.github.mikeldpl.ca.core.composer.melody.accompaniment.Accompaniment;
import com.github.mikeldpl.ca.core.shell.sound.SoundInfo;
import com.github.mikeldpl.ca.core.composer.harmony.Harmony;
import com.github.mikeldpl.ca.core.composer.melody.MelodyExtractor;

public interface Composer {
    SoundInfo compose(CommandComposer commandComposer);
    void setTact(int tact);
    void setDuration(long duration);
    void setMelodyExtractor(MelodyExtractor melodyExtractor);
    void setAccompaniment(Accompaniment accompaniment);
    void setStage(Note stage);
    void setHarmony(Harmony harmony);
    void clean();
}
