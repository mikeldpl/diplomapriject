package com.github.mikeldpl.ca.core.src;

import com.github.mikeldpl.ca.core.ca.Ca;
import com.github.mikeldpl.ca.core.extractor.Extractor;

public interface CaSource<EXT extends Extractor> {

    Ca getCa();

    void setCa(Ca ca);

    EXT getExtractor();

    void setExtractor(EXT ex);

    Class<EXT> getExtractorType();

    void close();
}

