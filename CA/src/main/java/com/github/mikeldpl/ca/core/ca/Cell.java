package com.github.mikeldpl.ca.core.ca;

public interface Cell {

    int getState();

    void setState(int value);

    void setDefaultState();

    int getMaxState();
}
