package com.github.mikeldpl.ca.core.dst.interpreter.strategy;

import com.github.mikeldpl.ca.core.ca.Ca6Area;
import com.github.mikeldpl.ca.core.composer.CommandComposer;
import com.github.mikeldpl.ca.core.ca.Ca6;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("State")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Ca6StatesToComposer extends Ca6ToComposerBase {

    protected StateAreaInfo prevStateInfo;

    @Override
    protected CommandComposer extractComposerCommand(Ca6Area area) {
        StateAreaInfo stateInfo = new StateAreaInfo(area);
        if (stateInfo.stateSum == 0) {
            prevStateInfo = null;
            return null;
        }
        CommandComposer command = new CommandComposer();
        command.setChord(getChord(stateInfo));
        command.setMove(getMove(stateInfo));
        command.setTemperament(getTemperament(stateInfo));
        prevStateInfo = stateInfo;
        return command;
    }

    protected double getTemperament(StateAreaInfo stateInfo) {
        return stateInfo.liveCells / (double) stateInfo.maxCells;
    }

    protected int getMove(StateAreaInfo stateInfo) {
        long change = getChange(stateInfo);
        return change > 0 ? 1 : change < 0 ? -1 : 0;
    }

    protected double getChord(StateAreaInfo stateInfo) {
        long change = getChange(stateInfo);
        return change == 0 ? 0 : Math.abs(change / (double) Math.max(prevStateInfo.stateSum, stateInfo.stateSum));
    }

    protected int getChange(StateAreaInfo stateInfo) {
        return prevStateInfo == null ? 0 : stateInfo.stateSum - prevStateInfo.stateSum;
    }

    protected static class StateAreaInfo {
        int stateSum;
        int liveCells;
        int maxCells;

        StateAreaInfo() {
        }

        StateAreaInfo(Ca6Area area) {
            maxCells = area.numberOfCells();
            for (Ca6.Cell6 cell6 : area) {
                if (cell6.getState() != 0) {
                    stateSum += cell6.getState();
                    liveCells++;
                }
            }
        }
    }
}
