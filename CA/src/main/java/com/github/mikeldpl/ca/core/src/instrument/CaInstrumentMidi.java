package com.github.mikeldpl.ca.core.src.instrument;

import com.github.mikeldpl.ca.core.extractor.ExtractorMidi;
import com.github.mikeldpl.ca.core.extractor.ExtractorPoly;
import com.github.mikeldpl.ca.core.shell.sound.SoundInfo;
import com.github.mikeldpl.ca.core.transformer.TransformerStrategyMidi;
import com.github.mikeldpl.ca.core.extractor.ExtractorPolyOneThread;
import com.github.mikeldpl.ca.core.extractor.MidiMetaMessageUtil;
import com.github.mikeldpl.ca.core.shell.sound.SoundShell;
import com.github.mikeldpl.ca.core.src.BaseCaSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CaInstrumentMidi extends BaseCaSource<ExtractorMidi> implements CaInstrument<ExtractorMidi, TransformerStrategyMidi> {

    private static final boolean USE_TRANSFORMATION = false;
    private boolean isTransformer;
    private TransformerStrategyMidi transformerStrategy;
    @Autowired
    @Qualifier("audioMidiShell")
    private SoundShell soundShell;

    @Override
    public void setExtractor(ExtractorMidi ex) {
        super.setExtractor(ex);
        if (ex instanceof ExtractorPolyOneThread && ex instanceof ExtractorPoly) {
            ((ExtractorPolyOneThread) ex).setMaxDelayBetweenNotesThatPlayedTogetherInTicks(Long.MAX_VALUE);
        }
    }

    @Override
    public void setInstrumentVoice(String instrument) {
        soundShell.setInstrumentVoice(instrument);
    }

    @Override
    public String[] getInstrumentVoices() {
        return soundShell.getInstrumentVoices();
    }

    @Override
    public TransformerStrategyMidi getTransformerStrategy() {
        return transformerStrategy;
    }

    @Override
    public void setTransformerStrategy(TransformerStrategyMidi transformerStrategy) {
        this.transformerStrategy = transformerStrategy;
    }

    @Override
    public Class<TransformerStrategyMidi> getStrategyType() {
        return TransformerStrategyMidi.class;
    }

    @Override
    public void setIsTransformer(boolean isTransformer) {
        this.isTransformer = isTransformer;
    }

    @Override
    public void keyDown(byte key) {
        if (USE_TRANSFORMATION) {
            key = transform(key, MidiMetaMessageUtil.NOTE_ON_MESSAGE);
        }
        soundShell.fire(SoundInfo.inst().notesOn(key),
                processedKey -> processCommandOnOffNote((byte) processedKey, MidiMetaMessageUtil.NOTE_ON_MESSAGE));
    }

    @Override
    public void keyUp(byte key) {
        if (USE_TRANSFORMATION) {
            key = transform(key, MidiMetaMessageUtil.NOTE_OFF_MESSAGE);
        }
        soundShell.fire(SoundInfo.inst().notesOff(key),
                processedKey -> processCommandOnOffNote((byte)processedKey, MidiMetaMessageUtil.NOTE_OFF_MESSAGE));
    }

    private byte transform(byte key, byte message) {
        if (isTransformer && transformerStrategy != null)
            return transformerStrategy.invert(key, message, System.currentTimeMillis(), getCa(), getExtractor());
        return key;
    }

    private void processCommandOnOffNote(byte key, byte extractorCommand) {
        getExtractor().extract(MidiMetaMessageUtil.getMetaMessage(extractorCommand, key, System.currentTimeMillis()));
    }

    @Override
    public void close() {
        super.close();
        soundShell.close();
    }

    @Override
    public Class<ExtractorMidi> getExtractorType() {
        return ExtractorMidi.class;
    }

    @Override
    public void mute(boolean isMuted) {
        soundShell.mute(isMuted);
    }
}
