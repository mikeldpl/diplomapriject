package com.github.mikeldpl.doubletomidconverter.players;

import com.github.mikeldpl.doubletomidconverter.suppliers.NoteSupplier;
import com.github.mikeldpl.doubletomidconverter.utils.NoteUtil;
import com.github.mikeldpl.doubletomidconverter.players.sound.SoundShell;

import java.util.Objects;

public class MonoPlayer implements PlayerFromNoteSupplier, AutoCloseable {

    private static final byte VELOCITY_DEFAULT = 59;
    private static final boolean SHOW_NOTES = true;
    private static final long TIME_BEFORE_RELEASE = 500;
    private static final long TIME_BEFORE_PRESS = 10;

    private final NoteSupplier noteSupplier;
    private final SoundShell soundShell;
    private long timeBeforeRelease = TIME_BEFORE_RELEASE;
    private long timeBeforePress = TIME_BEFORE_PRESS;

    public MonoPlayer(NoteSupplier noteSupplier, SoundShell soundShell) {
        Objects.requireNonNull(noteSupplier);
        Objects.requireNonNull(soundShell);
        this.noteSupplier = noteSupplier;
        this.soundShell = soundShell;
    }

    @Override
    public void play() {
        while (noteSupplier.hasNext()) {
            int nextNote = noteSupplier.getNextNote();
            if (SHOW_NOTES) {
                System.out.print(NoteUtil.noteToName(nextNote) + " - ");
            }
            soundShell.presAndReleaseKey(nextNote, VELOCITY_DEFAULT, timeBeforeRelease);
            soundShell.pause(timeBeforePress);
        }
    }

    @Override
    public void close() {
        soundShell.close();
    }
}
