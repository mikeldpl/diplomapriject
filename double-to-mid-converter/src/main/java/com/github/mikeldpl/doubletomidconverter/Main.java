package com.github.mikeldpl.doubletomidconverter;

import com.github.mikeldpl.doubletomidconverter.players.MonoPlayer;
import com.github.mikeldpl.doubletomidconverter.suppliers.NormalizedSupplier;
import com.github.mikeldpl.uifxconsole.UiFxConsole;
import com.github.mikeldpl.doubletomidconverter.players.sound.FileShell;
import com.github.mikeldpl.doubletomidconverter.suppliers.universal.MathConstant;
import com.github.mikeldpl.doubletomidconverter.suppliers.universal.UniversalDecimalSupplier;

import java.io.IOException;
import java.net.URISyntaxException;

public class Main {
    public static void main(String[] args) throws IOException, URISyntaxException {
        UiFxConsole.configure().title("double-to-mid-converter").start();

//        FileShell test1 = new FileShell("test1.mid");
//        test1.presAndReleaseKey(48, 100, 1000);
//        test1.presAndReleaseKey(48, 100, 10000);
//        test1.presAndReleaseKey(48, 100, 1000);
//        test1.close();

        writeConstantToFile("E - SLOW.mid", MathConstant.E);
        writeConstantToFile("LN2 - SLOW.mid", MathConstant.LN2);
        writeConstantToFile("SQRT2 - SLOW.mid", MathConstant.SQRT2);
        writeConstantToFile("PI - SLOW.mid", MathConstant.PI);
    }

    private static void writeConstantToFile(String pathToOutputFile, MathConstant constant) {
        try (MonoPlayer player =
                     new MonoPlayer(
                             new NormalizedSupplier(
                                     new UniversalDecimalSupplier(constant, 1000))
                             , new FileShell(pathToOutputFile))) {
            player.play();
        }
    }
}
