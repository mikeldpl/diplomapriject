package com.github.mikeldpl.doubletomidconverter.suppliers.universal;

import com.github.mikeldpl.doubletomidconverter.suppliers.NoteSupplier;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

public class UniversalDecimalSupplier implements NoteSupplier {

    private static final BigDecimal NUMBER_OF_TONES_BD = new BigDecimal(NUMBER_OF_TONES);

    private BigDecimal curValue;
    private int count;

    public UniversalDecimalSupplier(MathConstant constant, int count) {
        this(constant.getValue(), count);
    }

    public UniversalDecimalSupplier(MathConstant constant) {
        this(constant.getValue(), Integer.MAX_VALUE);
    }

    public UniversalDecimalSupplier(BigDecimal curValue, int count) {
        this.curValue = curValue;
        this.count = count;
    }

    public UniversalDecimalSupplier(String value, int count) {
        BigDecimal temp = new BigDecimal(value).abs();
        while (temp.compareTo(BigDecimal.ONE) > 0) {
            temp = temp.divide(NUMBER_OF_TONES_BD, RoundingMode.HALF_UP);
        }
        curValue = temp;
        this.count = count;
    }

    @Override
    public int getNextNote() {
//        BigInteger full = curValue.toBigInteger();
//        curValue = curValue.subtract(new BigDecimal(full));
//        curValue = curValue.multiply(NUMBER_OF_TONES_BD);
//        return full.intValue();
        BigDecimal multiply = curValue.multiply(NUMBER_OF_TONES_BD);
        BigInteger full = multiply.toBigInteger();
        curValue = multiply.subtract(new BigDecimal(full));
        count--;
        return full.intValue();
    }

    @Override
    public boolean hasNext() {
        return count > 0 && curValue.signum() != 0;
    }
}
