package com.github.mikeldpl.doubletomidconverter.suppliers.universal;

import com.github.mikeldpl.doubletomidconverter.Main;
import com.github.mikeldpl.doubletomidconverter.exceptions.DoubleToMidConverterException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public enum MathConstant {
    PI {
        @Override
        protected String getPath() {
            return "PI.txt";
        }
    }, E {
        @Override
        protected String getPath() {
            return "E.txt";
        }
    }, LN2 {
        @Override
        protected String getPath() {
            return "LN2.txt";
        }
    }, SQRT2 {
        @Override
        protected String getPath() {
            return "SQRT2.txt";
        }
    };

    private String cache;

    protected abstract String getPath();

    public String getValue() {
        if (cache != null) {
            return cache;
        }
        try {
            return cache = Files.readAllLines(Paths.get(Main.class.getResource(getPath()).toURI()))
                    .stream()
                    .collect(Collectors.joining());
        } catch (IOException | URISyntaxException e) {
            throw new DoubleToMidConverterException(e);
        }
    }
}
