package com.github.mikeldpl.doubletomidconverter.players.sound;

public interface SoundShell extends AutoCloseable {

    void presAndReleaseKey(int next, int velocity, long time);

    void pause(long timeBeforePress);

    void close();
}
