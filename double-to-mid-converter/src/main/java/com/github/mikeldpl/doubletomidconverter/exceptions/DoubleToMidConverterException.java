package com.github.mikeldpl.doubletomidconverter.exceptions;

public class DoubleToMidConverterException extends RuntimeException {
    public DoubleToMidConverterException() {
    }

    public DoubleToMidConverterException(String message) {
        super(message);
    }

    public DoubleToMidConverterException(String message, Throwable cause) {
        super(message, cause);
    }

    public DoubleToMidConverterException(Throwable cause) {
        super(cause);
    }
}
