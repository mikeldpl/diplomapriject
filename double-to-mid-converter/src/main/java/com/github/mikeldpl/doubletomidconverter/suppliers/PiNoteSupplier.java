package com.github.mikeldpl.doubletomidconverter.suppliers;

public class PiNoteSupplier implements NoteSupplier {

    private static final int NUMBER_OF_DIGITS = 1024;
    private final String piDigits;
    private int n = 0;

    public PiNoteSupplier() {
        this(NUMBER_OF_DIGITS);
    }

    public PiNoteSupplier(int numberOfDigits) {
        StringBuilder pi = new StringBuilder(numberOfDigits);
        int boxes = numberOfDigits * NUMBER_OF_TONES / 3;
        int reminders[] = new int[boxes];
        for (int i = 0; i < boxes; i++) {
            reminders[i] = 2;
        }
        int heldDigits = 0;
        for (int i = 0; i < numberOfDigits; i++) {
            int carriedOver = 0;
            int sum = 0;
            for (int j = boxes - 1; j >= 0; j--) {
                reminders[j] *= NUMBER_OF_TONES;
                sum = reminders[j] + carriedOver;
                int quotient = sum / (j * 2 + 1);
                reminders[j] = sum % (j * 2 + 1);
                carriedOver = quotient * j;
            }
            reminders[0] = sum % NUMBER_OF_TONES;
            int q = sum / NUMBER_OF_TONES;
            if (q == NUMBER_OF_TONES - 1) {
                heldDigits++;
            } else if (q == NUMBER_OF_TONES) {
                q = 0;
                for (int k = 1; k <= heldDigits; k++) {
                    int replaced = Integer.parseInt(pi.substring(i - k, i - k + 1), NUMBER_OF_TONES);
                    if (replaced == NUMBER_OF_TONES - 1) {
                        replaced = 0;
                    } else {
                        replaced++;
                    }
                    pi.deleteCharAt(i - k);
                    pi.insert(i - k, replaced);
                }
                heldDigits = 1;
            } else {
                heldDigits = 1;
            }
            pi.append(Integer.toString(q, NUMBER_OF_TONES));
        }
        piDigits = pi.toString();
    }

    @Override
    public int getNextNote() {
        return Character.digit(piDigits.charAt(n++), NUMBER_OF_TONES);
    }

    @Override
    public boolean hasNext() {
        return n < piDigits.length();
    }
}
