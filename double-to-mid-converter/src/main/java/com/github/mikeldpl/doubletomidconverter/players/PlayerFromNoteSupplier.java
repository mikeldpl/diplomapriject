package com.github.mikeldpl.doubletomidconverter.players;

public interface PlayerFromNoteSupplier extends AutoCloseable {

    void play();

}
