package com.github.mikeldpl.doubletomidconverter.utils;

public final class NoteUtil {

    private static String[] noteNames = {"до", "до#", "ре", "ре#", "ми", "фа", "фа#", "соль", "соль#", "ля", "ля#", "си"};

    private NoteUtil() {
    }

    public static String noteToName(int n) {
        return noteNames[n % noteNames.length];
    }
}
