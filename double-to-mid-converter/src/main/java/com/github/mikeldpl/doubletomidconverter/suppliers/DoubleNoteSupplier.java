package com.github.mikeldpl.doubletomidconverter.suppliers;

public class DoubleNoteSupplier implements NoteSupplier {
    private double value;

    public DoubleNoteSupplier(double value) {
        this.value = value;
    }

    @Override
    public int getNextNote() {
        int res = (int) value;
        value -= res;
        value *= NUMBER_OF_TONES;
        return res;
    }

    @Override
    public boolean hasNext() {
        return true;
    }
}
