package com.github.mikeldpl.doubletomidconverter.suppliers;

public abstract class FilteredSupplier implements NoteSupplier {

    private NoteSupplier source;

    public FilteredSupplier(NoteSupplier source) {
        this.source = source;
    }

    public NoteSupplier getSource() {
        return source;
    }
}
