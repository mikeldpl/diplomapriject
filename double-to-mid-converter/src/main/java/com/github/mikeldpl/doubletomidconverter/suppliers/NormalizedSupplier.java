package com.github.mikeldpl.doubletomidconverter.suppliers;

public class NormalizedSupplier extends FilteredSupplier {

    private static final byte DEFAULT_OCTAVE = 48;

    public NormalizedSupplier(NoteSupplier source) {
        super(source);
    }

    @Override
    public int getNextNote() {
        return getSource().getNextNote() % NUMBER_OF_TONES + DEFAULT_OCTAVE;
    }

    @Override
    public boolean hasNext() {
        return getSource().hasNext();
    }
}
