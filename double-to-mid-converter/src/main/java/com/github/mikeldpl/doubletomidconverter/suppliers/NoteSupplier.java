package com.github.mikeldpl.doubletomidconverter.suppliers;

public interface NoteSupplier {
    int NUMBER_OF_TONES = 12;

    int getNextNote();

    boolean hasNext();
}
