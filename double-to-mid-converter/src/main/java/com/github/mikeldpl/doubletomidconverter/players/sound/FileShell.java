package com.github.mikeldpl.doubletomidconverter.players.sound;

import com.github.mikeldpl.doubletomidconverter.exceptions.DoubleToMidConverterException;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.SysexMessage;
import javax.sound.midi.Track;
import java.io.File;
import java.io.IOException;

public class FileShell implements SoundShell {

    private static final int NOTE_ON = 0b10010000;
    private static final int NOTE_OFF = 0b10000000;
    private final Sequence sequence;
    private final File pathToOutputFile;
    private final Track track;
    private long curTick = 1;

    public FileShell(String pathToOutputFile) {
        try {
            this.pathToOutputFile = new File(pathToOutputFile);
            this.sequence = new Sequence(Sequence.PPQ, 200);
            this.track = sequence.createTrack();
            setupBaseEvents(track);
        } catch (InvalidMidiDataException e) {
            throw new DoubleToMidConverterException(e);
        }
    }

    @Override
    public void presAndReleaseKey(int next, int velocity, long time) {
        track.add(createMidiEvent(next, velocity, NOTE_ON, curTick));
        curTick += time;
        track.add(createMidiEvent(next, velocity, NOTE_OFF, curTick));
    }

    private MidiEvent createMidiEvent(int next, int velocity, int com, long curTick) {
        return new MidiEvent(getShortMessage(next, velocity, com), curTick);
    }

    private ShortMessage getShortMessage(int next, int velocity, int noteOff) {
        try {
            return new ShortMessage(noteOff, next, velocity);
        } catch (InvalidMidiDataException e) {
            throw new DoubleToMidConverterException(e);
        }
    }

    @Override
    public void pause(long timeBeforePress) {
        curTick += timeBeforePress;
    }

    @Override
    public void close() {
        try {
            setupEndEvents(track);
            MidiSystem.write(sequence, 1, pathToOutputFile);
        } catch (IOException e) {
            throw new DoubleToMidConverterException(e);
        }
    }

    private void setupEndEvents(Track t) {
        try {
            t.add(new MidiEvent(new MetaMessage(0x2F, new byte[]{}, 0), curTick));
        } catch (InvalidMidiDataException e) {
            throw new DoubleToMidConverterException(e);
        }
    }

    private void setupBaseEvents(Track t) {
        //****  General MIDI sysex -- turn on General MIDI sound set  ****
        try {
            byte[] b = {(byte) 0xF0, 0x7E, 0x7F, 0x09, 0x01, (byte) 0xF7};
            SysexMessage sm = new SysexMessage();
            sm.setMessage(b, 6);
            MidiEvent me = new MidiEvent(sm, (long) 0);
            t.add(me);

//****  set tempo (meta event)  ****
            MetaMessage mt = new MetaMessage();
            byte[] bt = {0x02, 0x00, 0x00};
            mt.setMessage(0x51, bt, 3);
            me = new MidiEvent(mt, (long) 0);
            t.add(me);

//****  set track name (meta event)  ****
            mt = new MetaMessage();
            String TrackName = pathToOutputFile.getName();
            mt.setMessage(0x03, TrackName.getBytes(/*"UTF8"*/), TrackName.length());//TODO
            me = new MidiEvent(mt, (long) 0);
            t.add(me);

//****  set omni on  ****
            ShortMessage mm = new ShortMessage();
            mm.setMessage(0xB0, 0x7D, 0x00);
            me = new MidiEvent(mm, (long) 0);
            t.add(me);

//****  set poly on  ****
            mm = new ShortMessage();
            mm.setMessage(0xB0, 0x7F, 0x00);
            me = new MidiEvent(mm, (long) 0);
            t.add(me);

//****  set instrument to Piano  ****
            mm = new ShortMessage();
            mm.setMessage(0xC0, 0x00, 0x00);
            me = new MidiEvent(mm, (long) 0);
            t.add(me);

        } catch (InvalidMidiDataException e) {
            throw new DoubleToMidConverterException(e);
        }
    }
}
