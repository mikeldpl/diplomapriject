package com.github.mikeldpl.doubletomidconverter.players.sound;

import com.github.mikeldpl.doubletomidconverter.exceptions.DoubleToMidConverterException;

import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;

public class AudioShell implements SoundShell {

    private final MidiChannel channel;
    private final Synthesizer synth;

    public AudioShell() {
        try {
            synth = MidiSystem.getSynthesizer();
            synth.open();
            channel = synth.getChannels()[0];
        } catch (MidiUnavailableException e) {
            throw new DoubleToMidConverterException(e);
        }
    }

    @Override
    public void presAndReleaseKey(int note, int velocity, long time) {
        try {
            channel.noteOn(note, velocity);
            Thread.sleep(time);
            channel.noteOff(note);
        } catch (InterruptedException e) {
            throw new DoubleToMidConverterException(e);
        }
    }

    @Override
    public void pause(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            throw new DoubleToMidConverterException(e);
        }
    }

    @Override
    public void close() {
        synth.close();
    }
}
